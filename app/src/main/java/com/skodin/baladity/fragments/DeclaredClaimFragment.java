package com.skodin.baladity.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.skodin.baladity.R;
import com.skodin.baladity.adapters.DeclaredClaimAdapter;
import com.skodin.baladity.adapters.OnLoadMoreListener;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.models.Paginate;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.services.BaladityAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DeclaredClaimFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.recycler_my_reclamation)
    RecyclerView recyclerView;
    @Bind(R.id.data)
    RelativeLayout relativeLayout;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    DeclaredClaimAdapter adapter;
    List<Reclamation> reclamationList;
    int current_page = 1;
    protected Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_declared_claim, container, false);
        ButterKnife.bind(this, view);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        handler = new Handler();
        //swipe refresh
        swipeRefreshLayout.setColorSchemeResources(R.color.purple, R.color.teal, R.color.red);
        swipeRefreshLayout.setOnRefreshListener(this);
        reclamationList = new ArrayList<>();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        adapter = new DeclaredClaimAdapter(getActivity(), reclamationList, recyclerView, relativeLayout);
        recyclerView.setAdapter(adapter);

        if (Helper.testConnexionInternet(getActivity())) {
            callServiceExterne();
        } else {
            Helper.toast(getActivity(), getResources().getString(R.string.error_connexion));
        }

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (reclamationList.size() > 9) {
                    reclamationList.add(null);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyItemInserted(reclamationList.size() - 1);
                        }
                    });

                    current_page++;
                    if (Helper.testConnexionInternet(getActivity())) {
                        BaladityAPI service = ConfService.getRetrofit(getActivity()).create(BaladityAPI.class);
                        Call<Paginate> callService = service.getMyReclamations(Helper.getPreferenceCitoyen(getActivity()).getId(), current_page);
                        callService.enqueue(new Callback<Paginate>() {
                            @Override
                            public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                                if (response.isSuccess()) {
                                    try {
                                        reclamationList.remove(reclamationList.size() - 1);
                                        adapter.notifyItemRemoved(reclamationList.size());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    for (int i = 0; i < response.body().getData().size(); i++) {
                                        Reclamation reclamation = new Reclamation();
                                        reclamation.setModifications(response.body().getData().get(i).getModifications());
                                        reclamation.setDescription(response.body().getData().get(i).getDescription());
                                        reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                                        reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                                        reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                                        reclamation.setId(response.body().getData().get(i).getId());
                                        reclamation.setDegree(response.body().getData().get(i).getDegree());
                                        reclamation.setFlag(response.body().getData().get(i).getFlag());
                                        reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                                        reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                                        reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                                        reclamation.setIdCategorieProleme(response.body().getData().get(i).getIdCategorieProleme());
                                        reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                                        reclamationList.add(reclamation);
                                    }
                                    adapter.notifyDataSetChanged();
                                    adapter.setLoaded();

                                    if (reclamationList.size() == 0) {
                                        relativeLayout.setVisibility(View.VISIBLE);
                                    } else {
                                        relativeLayout.setVisibility(View.GONE);
                                    }
                                } else {
                                    if (reclamationList.size() == 0) {
                                        relativeLayout.setVisibility(View.VISIBLE);
                                    } else {
                                        relativeLayout.setVisibility(View.GONE);
                                    }
                                    Helper.toast(getActivity(), getResources().getString(R.string.errorOperation));
                                }
                            }

                            @Override
                            public void onFailure(Call<Paginate> call, Throwable t) {
                                if (reclamationList.size() == 0) {
                                    relativeLayout.setVisibility(View.VISIBLE);
                                } else {
                                    relativeLayout.setVisibility(View.GONE);
                                }
                                Helper.toast(getActivity(), getResources().getString(R.string.request_invalid));
                            }
                        });
                    } else {
                        Helper.toast(getActivity(), getResources().getString(R.string.error_connexion));
                    }
                }
            }
        });


        return view;
    }


    private void callServiceExterne() {
        current_page=1;
        final ProgressDialog mProgressDialog = Helper.showProgress(getActivity());
        BaladityAPI service = ConfService.getRetrofit(getActivity()).create(BaladityAPI.class);
        Call<Paginate> callService = service.getMyReclamations(Helper.getPreferenceCitoyen(getActivity()).getId(),current_page);
        callService.enqueue(new Callback<Paginate>() {
            @Override
            public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                if (response.isSuccess()) {
                    reclamationList.clear();
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Reclamation reclamation = new Reclamation();
                        reclamation.setModifications(response.body().getData().get(i).getModifications());
                        reclamation.setDescription(response.body().getData().get(i).getDescription());
                        reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                        reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                        reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                        reclamation.setId(response.body().getData().get(i).getId());
                        reclamation.setDegree(response.body().getData().get(i).getDegree());
                        reclamation.setFlag(response.body().getData().get(i).getFlag());
                        reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                        reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                        reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                        reclamation.setIdCategorieProleme(response.body().getData().get(i).getIdCategorieProleme());
                        reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                        reclamationList.add(reclamation);
                    }
                    adapter.notifyDataSetChanged();

                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (reclamationList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (reclamationList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(getActivity(), getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Paginate> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (reclamationList.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(getActivity(), getResources().getString(R.string.request_invalid));
            }
        });
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        current_page = 1;
        BaladityAPI service = ConfService.getRetrofit(getActivity()).create(BaladityAPI.class);
        Call<Paginate> callService = service.getMyReclamations(Helper.getPreferenceCitoyen(getActivity()).getId(),current_page);
        callService.enqueue(new Callback<Paginate>() {
            @Override
            public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                if (response.isSuccess()) {
                    reclamationList.clear();
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Reclamation reclamation = new Reclamation();
                        reclamation.setModifications(response.body().getData().get(i).getModifications());
                        reclamation.setDescription(response.body().getData().get(i).getDescription());
                        reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                        reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                        reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                        reclamation.setId(response.body().getData().get(i).getId());
                        reclamation.setDegree(response.body().getData().get(i).getDegree());
                        reclamation.setFlag(response.body().getData().get(i).getFlag());
                        reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                        reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                        reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                        reclamation.setIdCategorieProleme(response.body().getData().get(i).getIdCategorieProleme());
                        reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                        reclamationList.add(reclamation);
                    }
                    adapter.notifyDataSetChanged();

                    if (reclamationList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    if (reclamationList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Paginate> call, Throwable t) {
                if (reclamationList.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                // Helper.toast(getActivity(), getResources().getString(R.string.request_invalid));
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        if (Helper.testConnexionInternet(getActivity())) {
            refresh();
        } else {
            Helper.toast(getActivity(), getResources().getString(R.string.error_connexion));
        }
    }


    private void refresh() {
        current_page=1;
        BaladityAPI service = ConfService.getRetrofit(getActivity()).create(BaladityAPI.class);
        Call<Paginate> callService = service.getMyReclamations(Helper.getPreferenceCitoyen(getActivity()).getId(), current_page);
        callService.enqueue(new Callback<Paginate>() {
            @Override
            public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                if (response.isSuccess()) {
                    reclamationList.clear();
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Reclamation reclamation = new Reclamation();
                        reclamation.setModifications(response.body().getData().get(i).getModifications());
                        reclamation.setDescription(response.body().getData().get(i).getDescription());
                        reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                        reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                        reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                        reclamation.setId(response.body().getData().get(i).getId());
                        reclamation.setDegree(response.body().getData().get(i).getDegree());
                        reclamation.setFlag(response.body().getData().get(i).getFlag());
                        reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                        reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                        reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                        reclamation.setIdCategorieProleme(response.body().getData().get(i).getIdCategorieProleme());
                        reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                        reclamationList.add(reclamation);
                    }
                    adapter.notifyDataSetChanged();

                    if (reclamationList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (reclamationList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(getActivity(), getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Paginate> call, Throwable t) {
                if (reclamationList.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(getActivity(), getResources().getString(R.string.request_invalid));
            }
        });
    }
}
