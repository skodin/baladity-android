package com.skodin.baladity.fragments;


import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.skodin.baladity.R;

public class PrefsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
    }
}