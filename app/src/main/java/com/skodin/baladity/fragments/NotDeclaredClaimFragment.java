package com.skodin.baladity.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.orm.SugarContext;
import com.skodin.baladity.R;
import com.skodin.baladity.adapters.NotDeclaredClaimAdapter;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.services.BaladityAPI;
import com.skodin.baladity.services.ServiceInterne;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotDeclaredClaimFragment extends Fragment {

    @Bind(R.id.recycler_non_declarer)
    RecyclerView recyclerView;
    @Bind(R.id.data)
    RelativeLayout relativeLayout;
    @Bind(R.id.all_declare)
    Button allDeclareButton;
    NotDeclaredClaimAdapter adapter;
    List<Claim> claimList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_not_declared_claim, container, false);
        ButterKnife.bind(this, view);
        SugarContext.init(getContext());


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        claimList = new ArrayList<>();

        adapter = new NotDeclaredClaimAdapter(getActivity(), claimList,allDeclareButton,relativeLayout);
        callServiceInterne();

        return view;
    }

    private void callServiceInterne() {
        claimList.clear();
        adapter.notifyDataSetChanged();

        for (int i = 0; i < ServiceInterne.getAllClaim().size(); i++) {
            claimList.add(ServiceInterne.getAllClaim().get(i));
        }

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if(claimList.size() == 0){
            allDeclareButton.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);

        }else {
            relativeLayout.setVisibility(View.GONE);
            allDeclareButton.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.all_declare)
    public void declare(View view) {
            if (Helper.testConnexionInternet(getActivity())) {
               callServiceExterne(claimList);
            } else {
                Helper.toast(getActivity(), getResources().getString(R.string.error_connexion));
            }

    }

    public void callServiceExterne(final  List<Claim> claimList) {
        final ProgressDialog mProgressDialog = Helper.showProgress(getActivity());
        BaladityAPI service = ConfService.getRetrofit(getActivity()).create(BaladityAPI.class);

        for (int i=0;i<claimList.size();i++){
            final Claim  claim=claimList.get(i);
            final int position =i;
            Map<String, RequestBody> map = new HashMap<>();
            try {
                if (claim.getPictures().get(0).getChemin() != null) {
                    Uri file1Uri = Uri.parse(claim.getPictures().get(0).getChemin());
                    map.put("photo1\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file1Uri.getPath())));
                }
                if (claim.getPictures().get(1).getChemin() != null) {
                    Uri file2Uri = Uri.parse(claim.getPictures().get(1).getChemin());
                    map.put("photo2\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file2Uri.getPath())));
                }
                if (claim.getPictures().get(2).getChemin() != null) {
                    Uri file3Uri = Uri.parse(claim.getPictures().get(2).getChemin());
                    map.put("photo3\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file3Uri.getPath())));
                }
                if (claim.getPictures().get(3).getChemin() != null) {
                    Uri file4Uri = Uri.parse(claim.getPictures().get(3).getChemin());
                    map.put("photo4\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file4Uri.getPath())));
                }
                if (claim.getPictures().get(4).getChemin() != null) {
                    Uri file5Uri = Uri.parse(claim.getPictures().get(4).getChemin());
                    map.put("photo5\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file5Uri.getPath())));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            map.put("degree", ConfService.toRequestBody(String.valueOf(claim.getDegree())));
            map.put("description", ConfService.toRequestBody(claim.getDescription()));
            map.put("longitude", ConfService.toRequestBody(String.valueOf(claim.getLongitude())));
            map.put("latitude", ConfService.toRequestBody(String.valueOf(claim.getLatitude())));
            map.put("flag", ConfService.toRequestBody(String.valueOf(claim.getFlag())));
            map.put("idCitoyen", ConfService.toRequestBody(String.valueOf(Helper.getPreferenceCitoyen(getActivity()).getId())));
            map.put("idMunicipalite", ConfService.toRequestBody("" + claim.getIdMunicipalite()));
            map.put("idCategorieProleme", ConfService.toRequestBody("" + claim.getIdCategorieProleme()));
            Call<Reclamation> callService = service.declarer(map);
            callService.enqueue(new Callback<Reclamation>() {
                @Override
                public void onResponse(Call<Reclamation> call, Response<Reclamation> response) {

                    if(position== claimList.size()-1){
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                    }

                    if (response.isSuccess()) {
                        ServiceInterne.delete(claim.getId());
                        try {
                            for (int j = 0; j < 5; j++) {
                                Sdcard.deletePictureFromStorage(Uri.parse(claim.getPictures().get(j).getChemin()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Helper.toast(getActivity(), getActivity().getResources().getString(R.string.success));
                        if(position == claimList.size()-1){
                            notiferAdapter();
                        }
                    } else {
                        Helper.toast(getActivity(), getActivity().getResources().getString(R.string.errorOperation));
                    }
                }

                @Override
                public void onFailure(Call<Reclamation> call, Throwable t) {
                    if(position== claimList.size()-1){
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                    }
                    Helper.toast(getActivity(), getActivity().getResources().getString(R.string.problem_connexion));
                }
            });
        }
    }

    public void notiferAdapter() {
        claimList.clear();
        adapter.notifyDataSetChanged();
        if (claimList.size() == 0) {
            allDeclareButton.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
        } else {
            relativeLayout.setVisibility(View.GONE);
            allDeclareButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        callServiceInterne();
    }



}
