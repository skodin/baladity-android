package com.skodin.baladity.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.orm.SugarContext;
import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.EmailValidator;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Citoyen;
import com.skodin.baladity.services.BaladityAPI;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    @Bind(R.id.editText_email)
    EditText editTextEmail;
    @Bind(R.id.editText_password)
    EditText editTextPassword;
    @Bind(R.id.input_email)
    TextInputLayout textInputLayoutEmail;
    @Bind(R.id.input_password)
    TextInputLayout textInputLayoutPassword;
   // @Bind(R.id.sign_in_button)
   // SignInButton signInButton;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        SugarContext.init(this);

        Sdcard.deleteAllPicturesUploaded(this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (Helper.testConnexionInternet(this)) {
            if (Helper.getPreferenceCitoyen(LoginActivity.this) != null) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        } else {
            startActivity(new Intent(LoginActivity.this, NotDeclaredActivity.class));
            finish();

        }


        //connect with google account
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

      /*  signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        signInButton.setColorScheme(SignInButton.COLOR_LIGHT);
        setGooglePlusButtonText(signInButton, getResources().getString(R.string.signin_with_google));
        */
    }



    @OnClick(R.id.button_login)
    public void login(View view) {
        if (errors()) {
            if (Helper.testConnexionInternet(LoginActivity.this)) {
                callServiceExterne();
            } else {
                Helper.toast(this, getResources().getString(R.string.error_connexion));
            }
        }
    }

    @OnClick(R.id.button_signin)
    public void inscription(View view) {
        startActivity(new Intent(this, SigninActivity.class));
        finish();
    }


  /*  @OnClick(R.id.sign_in_button)
    public void connecterGoogle(View v) {
        if (Helper.testConnexionInternet(this)) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } else {
            Helper.toast(this, getResources().getString(R.string.error_connexion));
        }
    }
    */


    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<Citoyen> callService = service.login(getDataFromInterface());
        callService.enqueue(new Callback<Citoyen>() {
            @Override
            public void onResponse(Call<Citoyen> call, Response<Citoyen> response) {
                if (response.isSuccess()) {
                    Citoyen citoyen = new Citoyen();
                    citoyen.setToken(response.body().getToken());
                    citoyen.setEmail(response.body().getEmail());
                    citoyen.setId(response.body().getId());
                    citoyen.setNom(response.body().getNom());
                    citoyen.setPrenom(response.body().getPrenom());
                    citoyen.setUsername(response.body().getUsername());
                    citoyen.setTelephone(response.body().getTelephone());
                    citoyen.setEstBlocker(response.body().getEstBlocker());
                    citoyen.setEstConfirmer(response.body().getEstConfirmer());
                    citoyen.setPhoto(response.body().getPhoto());
                    Helper.savePreferenceCitoyen(LoginActivity.this, citoyen);
                    finish();
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    Helper.toast(LoginActivity.this, getResources().getString(R.string.error_data));
                }
            }

            @Override
            public void onFailure(Call<Citoyen> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(LoginActivity.this, getResources().getString(R.string.problem_connexion));

            }
        });

    }


    public Boolean errors() {
        boolean testEmail = false, testPassword = false;
        if (new EmailValidator().validate(editTextEmail.getText().toString())) {
            testEmail = true;
            textInputLayoutEmail.setErrorEnabled(false);
            editTextEmail.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);

            if (!editTextPassword.getText().toString().isEmpty()) {
                testPassword = true;
                textInputLayoutPassword.setErrorEnabled(false);
                editTextPassword.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);

            } else {
                textInputLayoutPassword.setError(getResources().getString(R.string.error_password));
                editTextPassword.requestFocus();
            }
        } else {
            textInputLayoutEmail.setError(getResources().getString(R.string.error_email));
            editTextEmail.requestFocus();
        }
        return testEmail && testPassword;
    }


    public Citoyen getDataFromInterface() {
        Citoyen citoyen = new Citoyen();
        citoyen.setEmail(editTextEmail.getText().toString());
        citoyen.setPassword(editTextPassword.getText().toString());
        return citoyen;
    }


    protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);
            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setTextSize(15);
                tv.setTypeface(null, Typeface.NORMAL);
                tv.setText(buttonText);
                return;
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
           // Toast.makeText(LoginActivity.this, acct.getEmail(), Toast.LENGTH_SHORT).show();
        } else {
            Helper.toast(this, getResources().getString(R.string.errorOperation));
        }
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
