package com.skodin.baladity.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.EmailValidator;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Citoyen;
import com.skodin.baladity.services.BaladityAPI;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SigninActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.editText_email)
    EditText editTextEmail;
    @Bind(R.id.editText_password)
    EditText editTextPassword;
    @Bind(R.id.editText_nom)
    EditText editTextNom;
    @Bind(R.id.editText_prenom)
    EditText editTextPrenom;
    @Bind(R.id.editText_username)
    EditText editTextNomUser;
    @Bind(R.id.editText_phone)
    EditText editTextPhone;
    @Bind(R.id.backgroundImageView)
    ImageView imageViewPhoto;
    @Bind(R.id.input_username)
    TextInputLayout textInputLayoutUserName;
    @Bind(R.id.input_email)
    TextInputLayout textInputLayoutEmail;
    @Bind(R.id.input_password)
    TextInputLayout textInputLayoutPassword;
    @Bind(R.id.show)
    CheckBox showVisible;


    private Uri fileUri, uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);

        try {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        showVisible.setOnCheckedChangeListener(this);

    }

    @OnClick(R.id.fab)
    public void takeImage(View v) {
        final CharSequence[] items = {getResources().getString(R.string.camera), getResources().getString(R.string.gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.take_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        dialog.dismiss();
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                            if (ContextCompat.checkSelfPermission(SigninActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(SigninActivity.this, new String[]{Manifest.permission.CAMERA}, DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA);
                                return;
                            }
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        uri = Uri.fromFile(Sdcard.getOutputMediaFile(SigninActivity.this));
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        startActivityForResult(intent,ConfService.REQUEST_IMAGE_CAMERA);
                        break;
                    case 1:
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, ConfService.REQUEST_IMAGE_CALLERY);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @OnClick(R.id.button_signin)
    public void inscription(View view) {
        if (errors()) {
            if (Helper.testConnexionInternet(SigninActivity.this)) {
                callServiceExterne();
            } else {
                Helper.toast(this, getResources().getString(R.string.error_connexion));
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent,ConfService.REQUEST_IMAGE_CAMERA);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ConfService.REQUEST_IMAGE_CALLERY){
            if (resultCode == RESULT_OK) {
                uri=data.getData();
            }
        }

        try {

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, option);

            String filename = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            fileUri = Sdcard.saveUploadedPicturesToSdcard(this,bitmap, filename);

            imageViewPhoto.setImageURI(fileUri);
        } catch (Exception e) {
            Helper.toast(this, getResources().getString(R.string.errorOperation));
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public Boolean errors() {
        boolean testUserName = false, testEmail = false, testPassword = false;
        if (!editTextNomUser.getText().toString().isEmpty()) {
            testUserName = true;
            textInputLayoutUserName.setErrorEnabled(false);
            editTextNomUser.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);

            if (new EmailValidator().validate(editTextEmail.getText().toString())) {
                testEmail = true;
                textInputLayoutEmail.setErrorEnabled(false);
                editTextEmail.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);

                if (!editTextPassword.getText().toString().isEmpty()) {
                    testPassword = true;
                    textInputLayoutPassword.setErrorEnabled(false);
                    editTextPassword.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);

                } else {
                    textInputLayoutPassword.setError(getResources().getString(R.string.error_password));
                    editTextPassword.requestFocus();
                }
            } else {
                textInputLayoutEmail.setError(getResources().getString(R.string.error_email));
                editTextEmail.requestFocus();
            }
        } else {
            textInputLayoutUserName.setError(getResources().getString(R.string.error_username));
            editTextNomUser.requestFocus();
        }

        return testUserName && testEmail && testPassword;
    }


    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(SigninActivity.this).create(BaladityAPI.class);
        Map<String, RequestBody> map = new HashMap<>();

        Citoyen c = getDataFromInterface();

        if (fileUri != null) {
            map.put("photo\"; filename=\"pp.png\" ", ConfService.toRequestBodyFile(new File(fileUri.getPath())));
        }
        map.put("username", ConfService.toRequestBody(c.getUsername()));
        map.put("nom", ConfService.toRequestBody(c.getNom()));
        map.put("prenom", ConfService.toRequestBody(c.getPrenom()));
        map.put("email", ConfService.toRequestBody(c.getEmail()));
        map.put("password", ConfService.toRequestBody(c.getPassword()));
        map.put("telephone", ConfService.toRequestBody(c.getTelephone()));


        Call<Citoyen> callService = service.register(map);
        callService.enqueue(new Callback<Citoyen>() {
            @Override
            public void onResponse(Call<Citoyen> call, Response<Citoyen> response) {
                if (response.isSuccess()) {
                    Citoyen citoyen = new Citoyen();
                    citoyen.setToken(response.body().getToken());
                    citoyen.setEmail(response.body().getEmail());
                    citoyen.setId(response.body().getId());
                    citoyen.setNom(response.body().getNom());
                    citoyen.setPrenom(response.body().getPrenom());
                    citoyen.setUsername(response.body().getUsername());
                    citoyen.setTelephone(response.body().getTelephone());
                    citoyen.setEstBlocker(response.body().getEstBlocker());
                    citoyen.setEstConfirmer(response.body().getEstConfirmer());
                    citoyen.setPhoto(response.body().getPhoto());
                    Helper.savePreferenceCitoyen(SigninActivity.this, citoyen);
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    Helper.toast(SigninActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Citoyen> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(SigninActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });

    }


    public Citoyen getDataFromInterface() {
        Citoyen citoyen = new Citoyen();
        citoyen.setUsername(editTextNomUser.getText().toString());
        citoyen.setNom(editTextNom.getText().toString());
        citoyen.setPrenom(editTextPrenom.getText().toString());
        citoyen.setEmail(editTextEmail.getText().toString());
        citoyen.setPassword(editTextPassword.getText().toString());
        citoyen.setTelephone(editTextPhone.getText().toString());
        return citoyen;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
