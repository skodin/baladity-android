package com.skodin.baladity.activities;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.adapters.ModificationAdapter;
import com.skodin.baladity.adapters.PhotoAdapter;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.DividerItemDecoration;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Modification;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    @Bind(R.id.textView_username)
    TextView textViewUsername;
    @Bind(R.id.textView_categorie)
    TextView textViewCategorie;
    @Bind(R.id.textView_description)
    TextView textViewDescription;

    @Bind(R.id.profile_photo)
    CircleImageView photoProfile;
    @Bind(R.id.recycler_all_modification)
    RecyclerView recyclerViewModification;


    @Bind(R.id.data)
    RelativeLayout relativeLayout;
    @Bind(R.id.pager)
    ViewPager viewPager;

    @Bind(R.id.frmlyout_locationnote_mapholder)
    FrameLayout frameLayout;

    List<Modification> listeModifications;
    ModificationAdapter modificationAdapter;
    int i = 0;
    String myObjectBundle;
    Reclamation reclamation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setupActionBar();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            myObjectBundle = extras.getString("reclamation");
        }
        reclamation = new Gson().fromJson(myObjectBundle, Reclamation.class);

        if (reclamation.getLatitude() == 0 || reclamation.getLongitude() == 0) {
            frameLayout.setVisibility(View.GONE);
        }


        PhotoAdapter adapter = new PhotoAdapter(this, reclamation.getPhotos());
        viewPager.setAdapter(adapter);

        try {
            textViewUsername.setText(Helper.ucfirst(reclamation.getCitoyen().getUser().getUsername()));
            textViewCategorie.setText(reclamation.getCategorieproleme().getNom());
            textViewDescription.setText(reclamation.getDescription());
            associatePhoto();
        } catch (Exception e) {
            e.printStackTrace();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewModification.setLayoutManager(layoutManager);
        recyclerViewModification.setItemAnimator(new DefaultItemAnimator());
        recyclerViewModification.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        recyclerViewModification.addItemDecoration(itemDecoration);
        recyclerViewModification.setNestedScrollingEnabled(false);

        listeModifications = new ArrayList<>();

        if (Helper.testConnexionInternet(this)) {
            callServiceExterne();
        } else {
            Helper.toast(this, getResources().getString(R.string.error_connexion));
        }

    }


    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void associatePhoto() {
        Uri uriPicture = Sdcard.checkExistenceFile(this, Helper.getFileName(ConfService.BASE_URL + reclamation.getCitoyen().getUser().getPhoto()), 0);
        //teste si image existe
        if (uriPicture != null) {
            photoProfile.setImageURI(uriPicture);
        } else {
            // download image and save
            Picasso.with(this).load(ConfService.BASE_URL + reclamation.getCitoyen().getUser().getPhoto())
                    .placeholder(R.drawable.photo)
                    .error(R.drawable.photo)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            try {
                                photoProfile.setImageBitmap(bitmap);
                                Sdcard.savePictures(DetailActivity.this, bitmap, Helper.getFileName(ConfService.BASE_URL + reclamation.getCitoyen().getUser().getPhoto()), 0);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            photoProfile.setImageDrawable(errorDrawable);
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            photoProfile.setImageDrawable(placeHolderDrawable);
                        }
                    });
        }
    }

    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<List<Modification>> callService = service.getModifications(reclamation.getId());
        callService.enqueue(new Callback<List<Modification>>() {
            @Override
            public void onResponse(Call<List<Modification>> call, Response<List<Modification>> response) {
                if (response.isSuccess()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        Modification modification = new Modification();
                        modification.setId(response.body().get(i).getId());
                        modification.setDate(response.body().get(i).getDate());
                        modification.setIdReclamation(response.body().get(i).getIdReclamation());
                        modification.setMessage(response.body().get(i).getMessage());
                        modification.setEtat(response.body().get(i).getEtat());
                        listeModifications.add(modification);
                    }
                    modificationAdapter = new ModificationAdapter(DetailActivity.this, listeModifications);
                    recyclerViewModification.setAdapter(modificationAdapter);
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (listeModifications.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (listeModifications.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(DetailActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<List<Modification>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (listeModifications.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(DetailActivity.this, getResources().getString(R.string.problem_connexion));

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GoogleMap mMap = googleMap;
        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL);
        options.zoomControlsEnabled(true);
        options.zoomGesturesEnabled(true);

        if (reclamation.getLatitude() == 0 || reclamation.getLongitude() == 0) {
            frameLayout.setVisibility(View.GONE);
        } else {

            LatLng marker = new LatLng(reclamation.getLatitude(), reclamation.getLongitude());
            mMap.addMarker(new MarkerOptions().position(marker).title(getResources().getString(R.string.exsite)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 14));
       }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
