package com.skodin.baladity.activities;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.skodin.baladity.R;
import com.skodin.baladity.adapters.CommentaireAdapter;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.DividerItemDecoration;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.models.Commentaire;
import com.skodin.baladity.models.User;
import com.skodin.baladity.services.BaladityAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {
    @Bind(R.id.recycler_comment)
    RecyclerView CommentRecycler;
    @Bind(R.id.text_comment)
    EditText editTextComment;
    @Bind(R.id.abuse)
    CheckBox checkBox;
    @Bind(R.id.data)
    RelativeLayout relativeLayout;

    int estAbuse = 0, idReclamation;
    List<Commentaire> commentaireList;
    CommentaireAdapter commentaireAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);

        setupActionBar();
        Bundle bundle = this.getIntent().getExtras();
        idReclamation = bundle.getInt("idReclamation", 1);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        CommentRecycler.setLayoutManager(layoutManager);
        CommentRecycler.setItemAnimator(new DefaultItemAnimator());
        CommentRecycler.setHasFixedSize(true);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        CommentRecycler.addItemDecoration(itemDecoration);

        commentaireList = new ArrayList<>();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    estAbuse = 1;
                } else {
                    estAbuse = 0;
                }
            }
        });

        if (Helper.testConnexionInternet(this)) {
            callServiceExterne();
        } else {
            Helper.toast(this, getResources().getString(R.string.error_connexion));
        }

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick(R.id.img_send)
    public void sendComment(View view) {
      //  Helper.toast(this,Helper.getDateSystem());
          if (errors()) {
            if (Helper.testConnexionInternet(CommentActivity.this)) {
                sendComment();
            } else {
                Helper.toast(this, getResources().getString(R.string.error_connexion));
            }
         }

    }


    public Boolean errors() {
        boolean testComment = false;
        if (!editTextComment.getText().toString().isEmpty()) {
            testComment = true;
        } else {
            Helper.toast(this,getResources().getString(R.string.error_comment));
            editTextComment.requestFocus();
        }
        return testComment;
    }


    private Commentaire getDataFromInterface() {
        Commentaire commentaire = new Commentaire();
        commentaire.setIdReclamation(idReclamation);
        commentaire.setIdUser(Helper.getPreferenceCitoyen(CommentActivity.this).getId());
        commentaire.setEstAbuse(estAbuse);
        commentaire.setMessage(editTextComment.getText().toString());
        commentaire.setDate(Helper.getDateSystem());
        return commentaire;
    }

    private void sendComment() {
        final ProgressDialog mProgressDialog = Helper.showProgress(CommentActivity.this);
        BaladityAPI service = ConfService.getRetrofit(CommentActivity.this).create(BaladityAPI.class);
        Call<Commentaire> callService = service.sendCommentaires(getDataFromInterface());
        callService.enqueue(new Callback<Commentaire>() {
            @Override
            public void onResponse(Call<Commentaire> call, Response<Commentaire> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                   // Helper.toast(CommentActivity.this, getResources().getString(R.string.success));
                    Commentaire commentaire = new Commentaire();
                    commentaire.setDate(Helper.getDateSystem());
                    commentaire.setMessage(editTextComment.getText().toString());
                    User user=new User();
                    user.setId(Helper.getPreferenceCitoyen(CommentActivity.this).getId());
                    user.setUsername(Helper.getPreferenceCitoyen(CommentActivity.this).getUsername());
                    commentaire.setUser(user);
                    commentaire.setIdUser(Helper.getPreferenceCitoyen(CommentActivity.this).getId());
                    commentaireList.add(commentaire);
                    commentaireAdapter.notifyDataSetChanged();
                    relativeLayout.setVisibility(View.GONE);
                    editTextComment.setText("");
                } else {
                    Helper.toast(CommentActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Commentaire> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(CommentActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(CommentActivity.this).create(BaladityAPI.class);
        Call<List<Commentaire>> callService = service.getCommentaires(idReclamation);
        callService.enqueue(new Callback<List<Commentaire>>() {
            @Override
            public void onResponse(Call<List<Commentaire>> call, Response<List<Commentaire>> response) {
                if (response.isSuccess()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        Commentaire commentaire = new Commentaire();
                        commentaire.setDate(response.body().get(i).getDate());
                        commentaire.setMessage(response.body().get(i).getMessage());
                        commentaire.setUser(response.body().get(i).getUser());
                        commentaire.setIdUser(response.body().get(i).getIdUser());
                        commentaireList.add(commentaire);
                    }
                    commentaireAdapter = new CommentaireAdapter(CommentActivity.this, commentaireList);
                    CommentRecycler.setAdapter(commentaireAdapter);
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (commentaireList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (commentaireList.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(CommentActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<List<Commentaire>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (commentaireList.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(CommentActivity.this, getResources().getString(R.string.problem_connexion));

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
