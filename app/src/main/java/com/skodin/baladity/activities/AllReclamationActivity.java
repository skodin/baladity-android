package com.skodin.baladity.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.skodin.baladity.R;
import com.skodin.baladity.adapters.AllClaimAdapter;
import com.skodin.baladity.adapters.OnLoadMoreListener;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.models.Etat;
import com.skodin.baladity.models.Paginate;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.services.BaladityAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllReclamationActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener {
    @Bind(R.id.recycler_all_reclamation)
    RecyclerView recyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.data)
    RelativeLayout relativeLayout;
    @Bind(R.id.search_view)
    MaterialSearchView searchView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.filter)
    MaterialSpinner materialSpinner;

    AllClaimAdapter adapter;
    List<Reclamation> listeReclamation;
    List<String> listDescriptions;
    SharedPreferences sharedPreferences;

    int current_page = 1;
    int flag = 0;
    int idEtat = 0;
    String textSearchRefresh = "";
    protected Handler handler;

    List<Etat> etatList;
    List<String> listeEtats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_reclamation);
        ButterKnife.bind(this);

        //instance of Handler
        handler = new Handler();
        //define sharedPreference
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //add colors to swipe Refresh
        swipeRefreshLayout.setColorSchemeResources(R.color.purple, R.color.teal, R.color.red);

        //setup toolbar
        try {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        etatList = new ArrayList<>();
        listeEtats = new ArrayList<>();

        //permission for access to webService
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //params of searchView
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.cursor);
        searchView.setEllipsize(true);

        fillSpinner();

        //declare List
        listDescriptions = new ArrayList<>();
        //params of RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        listeReclamation = new ArrayList<>();

        materialSpinner.setOnItemSelectedListener(this);
        //define adapter
        adapter = new AllClaimAdapter(AllReclamationActivity.this, listeReclamation, recyclerView);

        recyclerView.setAdapter(adapter);
        // setOnRefreshListener
        swipeRefreshLayout.setOnRefreshListener(this);

        //on load More pagination
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
              if (listeReclamation.size() > 9) {
                    listeReclamation.add(null);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyItemInserted(listeReclamation.size() - 1);
                        }
                    });
                    current_page++;
                    int idEtat = getEtat(listeEtats.get((int) materialSpinner.getSelectedItemId() - 1)).getId();
                    if (Helper.testConnexionInternet(AllReclamationActivity.this)) {
                        BaladityAPI service = ConfService.getRetrofit(AllReclamationActivity.this).create(BaladityAPI.class);
                        Call<Paginate> callService = service.filterByCategoryAndMunicipality(idEtat, Integer.parseInt(sharedPreferences.getString("key_categorie", "0")),  Helper.getPreferenceCitoyen(AllReclamationActivity.this).getId(), current_page);
                        callService.enqueue(new Callback<Paginate>() {
                            @Override
                            public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                                if (response.isSuccess()) {
                                    try {
                                        listeReclamation.remove(listeReclamation.size() - 1);
                                        adapter.notifyItemRemoved(listeReclamation.size());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    for (int i = 0; i < response.body().getData().size(); i++) {
                                        Reclamation reclamation = new Reclamation();
                                        reclamation.setId(response.body().getData().get(i).getId());
                                        reclamation.setDegree(response.body().getData().get(i).getDegree());
                                        reclamation.setDescription(response.body().getData().get(i).getDescription());
                                        reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                                        reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                                        reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                                        reclamation.setModifications(response.body().getData().get(i).getModifications());
                                        reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                                        reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                                        reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                                        reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                                        listDescriptions.add(response.body().getData().get(i).getDescription());
                                        listeReclamation.add(reclamation);
                                    }
                                    adapter.notifyDataSetChanged();
                                    adapter.setLoaded();
                                } else {
                                    Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.errorOperation));
                                }
                            }

                            @Override
                            public void onFailure(Call<Paginate> call, Throwable t) {
                                Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.problem_connexion));
                            }
                        });
                    } else {
                        Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.error_connexion));
                    }
                }
            }
        });

        // searchView events
        //  String arrayDescriptions[] = listDescriptions.toArray(new String[listDescriptions.size()]);
        // searchView.setSuggestions(arrayDescriptions);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                flag = 1;
                if (Helper.testConnexionInternet(AllReclamationActivity.this)) {
                    callServiceExterneSearch(query);
                } else {
                    Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.error_connexion));
                    flag = 0;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
            }

            @Override
            public void onSearchViewClosed() {
                if (flag == 0) {
                    if (Helper.testConnexionInternet(AllReclamationActivity.this)) {
                        callServiceExterne(idEtat);
                    } else {
                        Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.error_connexion));
                    }
                } else {
                    flag = 0;
                }
            }
        });
    }

    private void fillSpinner() {
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<List<Etat>> callService = service.getAllEtats();
        callService.enqueue(new Callback<List<Etat>>() {
            @Override
            public void onResponse(Call<List<Etat>> call, Response<List<Etat>> response) {
                List<Etat> liste = response.body();
                etatList.add(new Etat(0, "Tous"));
                listeEtats.add("Tous");
                for (int i = 0; i < liste.size(); i++) {
                    etatList.add(new Etat(liste.get(i).getId(), liste.get(i).getNom()));
                    listeEtats.add(liste.get(i).getNom());
                }
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(AllReclamationActivity.this, android.R.layout.simple_spinner_item, listeEtats);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                materialSpinner.setAdapter(spinnerAdapter);
                materialSpinner.setSelection(1);
            }

            @Override
            public void onFailure(Call<List<Etat>> call, Throwable t) {
                //
            }
        });
    }

    private void callServiceExterneSearch(final String query) {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<List<Reclamation>> callService = service.getReclamationsByDescription(query, Helper.getPreferenceCitoyen(AllReclamationActivity.this).getId());
        callService.enqueue(new Callback<List<Reclamation>>() {
            @Override
            public void onResponse(Call<List<Reclamation>> call, Response<List<Reclamation>> response) {
                if (response.isSuccess()) {
                    listeReclamation.clear();
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < response.body().size(); i++) {
                        Reclamation reclamation = new Reclamation();
                        reclamation.setId(response.body().get(i).getId());
                        reclamation.setDegree(response.body().get(i).getDegree());
                        reclamation.setDescription(response.body().get(i).getDescription());
                        reclamation.setLongitude(response.body().get(i).getLongitude());
                        reclamation.setLatitude(response.body().get(i).getLatitude());
                        reclamation.setPhotos(response.body().get(i).getPhotos());
                        reclamation.setModifications(response.body().get(i).getModifications());
                        reclamation.setCitoyen(response.body().get(i).getCitoyen());
                        reclamation.setCategorieproleme(response.body().get(i).getCategorieproleme());
                        reclamation.setDelegation(response.body().get(i).getDelegation());
                        reclamation.setIdMunicipalite(response.body().get(i).getIdMunicipalite());
                        listDescriptions.add(response.body().get(i).getDescription());
                        listeReclamation.add(reclamation);
                    }

                    recyclerView.setAdapter(adapter);
                    // adapter.notifyDataSetChanged();

                    searchView.showSearch(false);
                    searchView.setQuery(query, false);

                    textSearchRefresh = query;


                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (listeReclamation.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (listeReclamation.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<List<Reclamation>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (listeReclamation.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
            flag = 0;
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.all_claim, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    private void callServiceExterne(int idEtat) {
        current_page = 1;
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<Paginate> callService = service.filterByCategoryAndMunicipality(idEtat, Integer.parseInt(sharedPreferences.getString("key_categorie", "0")), Helper.getPreferenceCitoyen(AllReclamationActivity.this).getId(), current_page);
        callService.enqueue(new Callback<Paginate>() {
            @Override
            public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                if (response.isSuccess()) {
                    listeReclamation.clear();
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Reclamation reclamation = new Reclamation();
                        reclamation.setId(response.body().getData().get(i).getId());
                        reclamation.setDegree(response.body().getData().get(i).getDegree());
                        reclamation.setDescription(response.body().getData().get(i).getDescription());
                        reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                        reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                        reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                        reclamation.setModifications(response.body().getData().get(i).getModifications());
                        reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                        reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                        reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                        reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                        listDescriptions.add(response.body().getData().get(i).getDescription());
                        listeReclamation.add(reclamation);
                    }
                    adapter.notifyDataSetChanged();

                    textSearchRefresh = "";

                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (listeReclamation.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if (listeReclamation.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Paginate> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (listeReclamation.size() == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.problem_connexion));

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }
            return;
        } else if (requestCode == 9999) {
            // toolbar.setTitle(getResources().getString(R.string.all_claim));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        current_page = 1;
        if (textSearchRefresh.length() == 0) {
            int idEtat = getEtat(listeEtats.get((int) materialSpinner.getSelectedItemId() - 1)).getId();
            BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
            Call<Paginate> callService = service.filterByCategoryAndMunicipality(idEtat, Integer.parseInt(sharedPreferences.getString("key_categorie", "0")), Helper.getPreferenceCitoyen(AllReclamationActivity.this).getId(), current_page);
            callService.enqueue(new Callback<Paginate>() {
                @Override
                public void onResponse(Call<Paginate> call, Response<Paginate> response) {
                    if (response.isSuccess()) {
                        listeReclamation.clear();
                        adapter.notifyDataSetChanged();
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            Reclamation reclamation = new Reclamation();
                            reclamation.setId(response.body().getData().get(i).getId());
                            reclamation.setDegree(response.body().getData().get(i).getDegree());
                            reclamation.setDescription(response.body().getData().get(i).getDescription());
                            reclamation.setLongitude(response.body().getData().get(i).getLongitude());
                            reclamation.setLatitude(response.body().getData().get(i).getLatitude());
                            reclamation.setPhotos(response.body().getData().get(i).getPhotos());
                            reclamation.setModifications(response.body().getData().get(i).getModifications());
                            reclamation.setCitoyen(response.body().getData().get(i).getCitoyen());
                            reclamation.setCategorieproleme(response.body().getData().get(i).getCategorieproleme());
                            reclamation.setDelegation(response.body().getData().get(i).getDelegation());
                            reclamation.setIdMunicipalite(response.body().getData().get(i).getIdMunicipalite());
                            listDescriptions.add(response.body().getData().get(i).getDescription());
                            listeReclamation.add(reclamation);
                        }
                        adapter.notifyDataSetChanged();

                        if (listeReclamation.size() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayout.setVisibility(View.GONE);
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        if (listeReclamation.size() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayout.setVisibility(View.GONE);
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<Paginate> call, Throwable t) {
                    if (listeReclamation.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }

                }
            });
        } else {
            BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
            Call<List<Reclamation>> callService = service.getReclamationsByDescription(textSearchRefresh, Helper.getPreferenceCitoyen(AllReclamationActivity.this).getId());
            callService.enqueue(new Callback<List<Reclamation>>() {
                @Override
                public void onResponse(Call<List<Reclamation>> call, Response<List<Reclamation>> response) {
                    if (response.isSuccess()) {
                        listeReclamation.clear();
                        adapter.notifyDataSetChanged();
                        for (int i = 0; i < response.body().size(); i++) {
                            Reclamation reclamation = new Reclamation();
                            reclamation.setId(response.body().get(i).getId());
                            reclamation.setDegree(response.body().get(i).getDegree());
                            reclamation.setDescription(response.body().get(i).getDescription());
                            reclamation.setLongitude(response.body().get(i).getLongitude());
                            reclamation.setLatitude(response.body().get(i).getLatitude());
                            reclamation.setPhotos(response.body().get(i).getPhotos());
                            reclamation.setModifications(response.body().get(i).getModifications());
                            reclamation.setCitoyen(response.body().get(i).getCitoyen());
                            reclamation.setCategorieproleme(response.body().get(i).getCategorieproleme());
                            reclamation.setDelegation(response.body().get(i).getDelegation());
                            reclamation.setIdMunicipalite(response.body().get(i).getIdMunicipalite());
                            listDescriptions.add(response.body().get(i).getDescription());
                            listeReclamation.add(reclamation);
                        }

                        recyclerView.setAdapter(adapter);
                        // adapter.notifyDataSetChanged();

                        searchView.showSearch(false);
                        searchView.setQuery(textSearchRefresh, false);

                        if (listeReclamation.size() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayout.setVisibility(View.GONE);
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        if (listeReclamation.size() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                        } else {
                            relativeLayout.setVisibility(View.GONE);
                        }
                        Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.errorOperation));
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<List<Reclamation>> call, Throwable t) {
                    if (listeReclamation.size() == 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(AllReclamationActivity.this, getResources().getString(R.string.problem_connexion));
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    private Etat getEtat(String s) {
        for (int i = 0; i < etatList.size(); i++) {
            if (etatList.get(i).getNom().equals(s)) {
                return etatList.get(i);
            }
        }
        return null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        current_page = 1;
        try {
            idEtat = getEtat((String) materialSpinner.getItemAtPosition(position)).getId();
        } catch (Exception e) {
            materialSpinner.setSelection(1);
            idEtat = 0;
        }

        if (Helper.testConnexionInternet(this)) {
            callServiceExterne(idEtat);
        } else {
            Helper.toast(this, getResources().getString(R.string.error_connexion));
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
