package com.skodin.baladity.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.orm.SugarContext;
import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Categoriesprobleme;
import com.skodin.baladity.models.Municipalite;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.sugar.Category;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.models.sugar.Municipality;
import com.skodin.baladity.models.sugar.Picture;
import com.skodin.baladity.services.BaladityAPI;
import com.skodin.baladity.services.ServiceInterne;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeclarerActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 12;

    @Bind(R.id.flag)
    CheckBox checkBox;
    @Bind(R.id.editText_description)
    EditText editTextDescription;
    @Bind(R.id.input_description)
    TextInputLayout textInputLayoutDescription;
    @Bind(R.id.backgroundImageView)
    ImageView imageViewPhoto1;
    @Bind(R.id.photo2)
    ImageView imageViewPhoto2;
    @Bind(R.id.photo3)
    ImageView imageViewPhoto3;
    @Bind(R.id.photo4)
    ImageView imageViewPhoto4;
    @Bind(R.id.photo5)
    ImageView imageViewPhoto5;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.spinner_categorie)
    MaterialSpinner spinnerCategorie;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    private Uri file1Uri, file2Uri, file3Uri, file4Uri, file5Uri;
    int flag = 1;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Double[] myLocation = new Double[2];
    List<Categoriesprobleme> categoriesproblemeList;
    List<String> listeCategorieProbleme;
    List<Category> categoryList;


    static int click = 1;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declare);
        ButterKnife.bind(this);
        SugarContext.init(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        categoriesproblemeList = new ArrayList<>();
        listeCategorieProbleme = new ArrayList<>();
        categoryList = new ArrayList<>();


        fab.setOnClickListener(this);
        imageViewPhoto2.setOnClickListener(this);
        imageViewPhoto3.setOnClickListener(this);
        imageViewPhoto4.setOnClickListener(this);
        imageViewPhoto5.setOnClickListener(this);

        imageViewPhoto3.setVisibility(View.GONE);
        imageViewPhoto4.setVisibility(View.GONE);
        imageViewPhoto5.setVisibility(View.GONE);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flag = 1;
                } else {
                    flag = 0;
                }
            }
        });

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        if (Helper.testConnexionInternet(this)) {
            fillSpinner();
        } else {
            categoryList = Category.listAll(Category.class);
            for (int i = 0; i < categoryList.size(); i++) {
                listeCategorieProbleme.add(categoryList.get(i).getNom());
            }
            ArrayAdapter<String> spinnerAdapter1 = new ArrayAdapter<String>(DeclarerActivity.this, android.R.layout.simple_spinner_item, listeCategorieProbleme);
            spinnerAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCategorie.setAdapter(spinnerAdapter1);
        }
    }

    private void fillSpinner() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);

        Call<List<Categoriesprobleme>> serviceCat = service.getAllCategorieProblemes();
        serviceCat.enqueue(new Callback<List<Categoriesprobleme>>() {
            @Override
            public void onResponse(Call<List<Categoriesprobleme>> call, Response<List<Categoriesprobleme>> response) {
                List<Categoriesprobleme> listCat = response.body();

                if (listCat != null) {
                    Category.deleteAll(Category.class);
                    for (int i = 0; i < listCat.size(); i++) {
                        categoriesproblemeList.add(new Categoriesprobleme(listCat.get(i).getId(), listCat.get(i).getNom()));
                        listeCategorieProbleme.add(listCat.get(i).getNom());
                        Category category = new Category();
                        category.setIdCategory(listCat.get(i).getId());
                        category.setNom(listCat.get(i).getNom());
                        category.save();
                    }
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(DeclarerActivity.this, android.R.layout.simple_spinner_item, listeCategorieProbleme);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerCategorie.setAdapter(spinnerAdapter);
                }

                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Categoriesprobleme>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(DeclarerActivity.this, getResources().getString(R.string.problem_connexion));

            }
        });


    }

    public void takeImage(View view) {
        final CharSequence[] items = {getResources().getString(R.string.camera), getResources().getString(R.string.gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.take_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        dialog.dismiss();
                        openCamera();
                        break;
                    case 1:
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void openCamera() {
        //test of version android
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            // if permission not granted
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                //open Dialog permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                // }
                return;
            }
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ConfService.REQUEST_IMAGE_CALLERY) {
            if (resultCode == RESULT_OK) {
                uri = data.getData();
            }
        }

        try {

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, option);

            String filename = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            Uri fileUri = Sdcard.saveUploadedPicturesToSdcard(this, bitmap, filename);

            switch (click) {
                case 1:
                    file1Uri = fileUri;
                    imageViewPhoto1.setImageURI(file1Uri);
                    break;
                case 2:
                    file2Uri = fileUri;
                    imageViewPhoto2.setImageURI(file2Uri);
                    imageViewPhoto3.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    file3Uri = fileUri;
                    imageViewPhoto3.setImageURI(file3Uri);
                    imageViewPhoto4.setVisibility(View.VISIBLE);
                    break;
                case 4:
                    file4Uri = fileUri;
                    imageViewPhoto4.setImageURI(file4Uri);
                    imageViewPhoto5.setVisibility(View.VISIBLE);
                    break;
                case 5:
                    file5Uri = fileUri;
                    imageViewPhoto5.setImageURI(file5Uri);
                    break;
            }

        } catch (Exception e) {
            Helper.toast(this, getResources().getString(R.string.errorOperation));
        }


    }

    private void callServiceInterne() {
        Claim claim = new Claim();
        claim.setFlag(flag);
        claim.setDegree(1);
        claim.setDescription(editTextDescription.getText().toString());
        claim.setLatitude(myLocation[0]);
        claim.setLongitude(myLocation[1]);
        claim.setIdCategorieProleme(getCategory(listeCategorieProbleme.get((int) spinnerCategorie.getSelectedItemId() - 1)).getIdCategory());

        Claim c = ServiceInterne.saveClaim(claim);
        if (file1Uri != null) {
            file1Uri = Sdcard.movefile(file1Uri, this);
            ServiceInterne.savePicture(new Picture(file1Uri.toString(), c));
        }
        if (file2Uri != null) {
            file2Uri = Sdcard.movefile(file2Uri, this);
            ServiceInterne.savePicture(new Picture(file2Uri.toString(), c));
        }
        if (file3Uri != null) {
            file3Uri = Sdcard.movefile(file3Uri, this);
            ServiceInterne.savePicture(new Picture(file3Uri.toString(), c));
        }
        if (file4Uri != null) {
            file4Uri = Sdcard.movefile(file4Uri, this);
            ServiceInterne.savePicture(new Picture(file4Uri.toString(), c));
        }
        if (file5Uri != null) {
            file5Uri = Sdcard.movefile(file5Uri, this);
            ServiceInterne.savePicture(new Picture(file5Uri.toString(), c));
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.button_declarer)
    public void declarer(View view) {
        if (errorsExterne()) {
            if (Helper.testConnexionInternet(DeclarerActivity.this)) {
                callServiceExterne();
            } else {
                callServiceInterne();
                Helper.toast(this, getResources().getString(R.string.success));
                onBackPressed();
            }
        }
    }


    private boolean errorsExterne() {
        boolean testDescription = false, testCategorie = false, testeImage = false;
        if (!editTextDescription.getText().toString().isEmpty()) {
            testDescription = true;
            textInputLayoutDescription.setErrorEnabled(false);
            editTextDescription.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            if (spinnerCategorie.getSelectedItemId() != 0) {
                testCategorie = true;
                spinnerCategorie.setError(null);
                spinnerCategorie.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);

                if (file1Uri != null || file2Uri != null || file3Uri != null || file4Uri != null || file5Uri != null) {
                    testeImage = true;
                } else {
                    Helper.toast(this, getResources().getString(R.string.add_image));
                }


            } else {
                spinnerCategorie.setError(getResources().getString(R.string.error_categorie));
                spinnerCategorie.requestFocus();
            }
        } else {
            textInputLayoutDescription.setError(getResources().getString(R.string.error_description));
            editTextDescription.requestFocus();
        }
        return testDescription && testCategorie && testeImage;
    }


    public void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(DeclarerActivity.this);
        BaladityAPI service = ConfService.getRetrofit(DeclarerActivity.this).create(BaladityAPI.class);
        Map<String, RequestBody> map = new HashMap<>();

        if (file1Uri != null) {
            map.put("photo1\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file1Uri.getPath())));
        }
        if (file2Uri != null) {
            map.put("photo2\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file2Uri.getPath())));
        }
        if (file3Uri != null) {
            map.put("photo3\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file3Uri.getPath())));
        }
        if (file4Uri != null) {
            map.put("photo4\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file4Uri.getPath())));
        }
        if (file5Uri != null) {
            map.put("photo5\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file5Uri.getPath())));
        }
        map.put("degree", ConfService.toRequestBody(String.valueOf(1)));
        map.put("description", ConfService.toRequestBody(editTextDescription.getText().toString()));
        map.put("flag", ConfService.toRequestBody(String.valueOf(flag)));
        map.put("idCitoyen", ConfService.toRequestBody(String.valueOf(Helper.getPreferenceCitoyen(DeclarerActivity.this).getId())));


        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (statusOfGPS) {
            map.put("longitude", ConfService.toRequestBody(String.valueOf(myLocation[1])));
            map.put("latitude", ConfService.toRequestBody(String.valueOf(myLocation[0])));
        }

        try {
            map.put("idCategorieProleme", ConfService.toRequestBody("" + getCategorieProbleme(listeCategorieProbleme.get((int) spinnerCategorie.getSelectedItemId() - 1)).getId()));
        } catch (Exception e) {
            Helper.toast(this, getResources().getString(R.string.error_categorie));
        }
        Call<Reclamation> callService = service.declarer(map);
        callService.enqueue(new Callback<Reclamation>() {
            @Override
            public void onResponse(Call<Reclamation> call, Response<Reclamation> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                    onBackPressed();
                } else {
                    Helper.toast(DeclarerActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Reclamation> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(DeclarerActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Categoriesprobleme getCategorieProbleme(String s) {
        for (int i = 0; i < categoriesproblemeList.size(); i++) {
            if (categoriesproblemeList.get(i).getNom().equals(s)) {
                return categoriesproblemeList.get(i);
            }
        }
        return null;
    }


    private Category getCategory(String s) {
        for (int i = 0; i < categoryList.size(); i++) {
            if (categoryList.get(i).getNom().equals(s)) {
                return categoryList.get(i);
            }
        }
        return null;
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 33);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation[0] = location.getLatitude();
        myLocation[1] = location.getLongitude();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                click = 1;
                break;
            case R.id.photo2:
                click = 2;
                break;
            case R.id.photo3:
                click = 3;
                break;
            case R.id.photo4:
                click = 4;
                break;
            case R.id.photo5:
                click = 5;
                break;
        }
        takeImage(v);
    }


}
