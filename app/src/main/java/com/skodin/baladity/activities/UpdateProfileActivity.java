package com.skodin.baladity.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.EmailValidator;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Citoyen;
import com.skodin.baladity.models.ResponseObject;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    @Bind(R.id.editText_email)
    EditText editTextEmail;
    @Bind(R.id.input_email)
    TextInputLayout textInputLayoutEmail;
    @Bind(R.id.editText_password)
    EditText editTextPassword;
    @Bind(R.id.input_password)
    TextInputLayout textInputLayoutPassword;
    @Bind(R.id.editText_nom)
    EditText editTextNom;
    @Bind(R.id.input_nom)
    TextInputLayout textInputLayoutNom;
    @Bind(R.id.editText_prenom)
    EditText editTextPrenom;
    @Bind(R.id.input_prenom)
    TextInputLayout textInputLayoutPrenom;
    @Bind(R.id.editText_username)
    EditText editTextNomUser;
    @Bind(R.id.input_username)
    TextInputLayout textInputLayoutUserName;
    @Bind(R.id.editText_phone)
    EditText editTextPhone;
    @Bind(R.id.input_phone)
    TextInputLayout textInputLayoutPhone;
    @Bind(R.id.backgroundImageView)
    ImageView imageViewPhoto;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.show)
    CheckBox showVisible;

    private Uri fileUri;
    Map<String, RequestBody> map;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        map = new HashMap<>();

        try {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (Exception e){
            e.printStackTrace();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeImage(view);
            }
        });

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        showVisible.setOnCheckedChangeListener(this);

        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent));
        editTextNom.setText(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getNom());
        editTextPrenom.setText(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getPrenom());
        editTextNomUser.setText(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getUsername());
        editTextEmail.setText(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getEmail());
        editTextPassword.setText(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getPassword());
        editTextPhone.setText(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getTelephone());


        Uri uriPicture = Sdcard.checkExistenceFile(this,Helper.getFileName(ConfService.BASE_URL + Helper.getPreferenceCitoyen(this).getPhoto()), 0);
        //teste si image existe
        if (uriPicture != null) {
            imageViewPhoto.setImageURI(uriPicture);
        } else {
            // download image and save
            Picasso.with(this).load(ConfService.BASE_URL + Helper.getPreferenceCitoyen(this).getPhoto())
            .placeholder(R.drawable.photo)
            .error(R.drawable.photo)
            .into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    try {
                        imageViewPhoto.setImageBitmap(bitmap);
                        Sdcard.savePictures(UpdateProfileActivity.this,bitmap, Helper.getFileName(ConfService.BASE_URL + Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getPhoto()), 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    imageViewPhoto.setImageDrawable(errorDrawable);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    imageViewPhoto.setImageDrawable(placeHolderDrawable);
                }
            });
        }


    }

    public void takeImage(View view) {
        final CharSequence[] items = {getResources().getString(R.string.camera),getResources().getString(R.string.gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.take_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        dialog.dismiss();
                        openCamera();
                        break;
                    case 1:
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, ConfService.REQUEST_IMAGE_CALLERY);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void openCamera() {
        //test of version android
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1){
            // if permission not granted
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                //open Dialog permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA);
                // }

                return;
            }
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent,ConfService.REQUEST_IMAGE_CAMERA);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == ConfService.REQUEST_IMAGE_CALLERY){
            if (resultCode == RESULT_OK) {
                uri=data.getData();
            }
        }

        try {

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, option);

            String filename = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            fileUri = Sdcard.saveUploadedPicturesToSdcard(this,bitmap, filename);

            imageViewPhoto.setImageURI(fileUri);

            callServiceUpdateImage();

        } catch (Exception e) {
            Helper.toast(this, getResources().getString(R.string.errorOperation));
        }
    }

    private void callServiceUpdateImage() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        RequestBody photo = RequestBody.create(MediaType.parse("image/*"), new File(fileUri.getPath()));
        RequestBody id = ConfService.toRequestBody(String.valueOf(Helper.getPreferenceCitoyen(this).getId()));
        Call<ResponseObject> callService = service.updateImageUser(id, photo);
        callService.enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                    Citoyen citoyen = new Citoyen();
                    citoyen.setToken(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getToken());
                    citoyen.setEmail(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getEmail());
                    citoyen.setId(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getId());
                    citoyen.setNom(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getNom());
                    citoyen.setPrenom(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getPrenom());
                    citoyen.setUsername(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getUsername());
                    citoyen.setTelephone(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getTelephone());
                    citoyen.setEstBlocker(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getEstBlocker());
                    citoyen.setEstConfirmer(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getEstConfirmer());
                    citoyen.setPhoto(response.body().getResult());

                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(UpdateProfileActivity.this);
                    final SharedPreferences.Editor editor1 = settings.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(citoyen);
                    editor1.putString("citoyen", json);
                    editor1.commit();

                    Helper.toast(UpdateProfileActivity.this, getResources().getString(R.string.success));
                } else {
                    Helper.toast(UpdateProfileActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(UpdateProfileActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @OnClick(R.id.button_update)
    public void update(View view) {
        if (errors()) {
            if (Helper.testConnexionInternet(UpdateProfileActivity.this)) {
                callServiceExterne();
            } else {
                Helper.toast(this, getResources().getString(R.string.error_connexion));
            }
        }
    }


    public Boolean errors() {
        boolean testUserName = false, testEmail = false;
              if (!editTextNomUser.getText().toString().isEmpty()) {
                    testUserName = true;
                    textInputLayoutUserName.setErrorEnabled(false);
                    editTextNomUser.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
                    if (new EmailValidator().validate(editTextEmail.getText().toString())) {
                        testEmail = true;
                        textInputLayoutEmail.setErrorEnabled(false);
                        editTextEmail.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
                    } else {
                        textInputLayoutEmail.setError(getResources().getString(R.string.error_email));
                        editTextEmail.requestFocus();
                    }
                } else {
                    textInputLayoutUserName.setError(getResources().getString(R.string.error_username));
                    editTextNomUser.requestFocus();
                }

        return  testUserName && testEmail;
    }

    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(UpdateProfileActivity.this).create(BaladityAPI.class);

        map.put("id", ConfService.toRequestBody(String.valueOf(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getId())));
        map.put("username", ConfService.toRequestBody(getDataFromInterface().getUsername()));
        map.put("nom", ConfService.toRequestBody(getDataFromInterface().getNom()));
        map.put("prenom", ConfService.toRequestBody(getDataFromInterface().getPrenom()));
        map.put("email", ConfService.toRequestBody(getDataFromInterface().getEmail()));
        if (editTextPassword.getText().toString().length() != 0) {
            map.put("password", ConfService.toRequestBody(getDataFromInterface().getPassword()));
        }
        map.put("telephone", ConfService.toRequestBody(getDataFromInterface().getTelephone()));

        Call<Citoyen> callService = service.updateCompte(map);
        callService.enqueue(new Callback<Citoyen>() {
            @Override
            public void onResponse(Call<Citoyen> call, Response<Citoyen> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                    Citoyen citoyen = new Citoyen();
                    citoyen.setToken(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getToken());
                    citoyen.setEmail(response.body().getEmail());
                    citoyen.setId(Helper.getPreferenceCitoyen(UpdateProfileActivity.this).getId());
                    citoyen.setNom(response.body().getNom());
                    citoyen.setPrenom(response.body().getPrenom());
                    citoyen.setUsername(response.body().getUsername());
                    citoyen.setTelephone(response.body().getTelephone());
                    citoyen.setEstBlocker(response.body().getEstBlocker());
                    citoyen.setEstConfirmer(response.body().getEstConfirmer());
                    citoyen.setPhoto(response.body().getPhoto());

                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(UpdateProfileActivity.this);
                    final SharedPreferences.Editor editor1 = settings.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(citoyen);
                    editor1.putString("citoyen", json);
                    editor1.commit();
                    Helper.toast(UpdateProfileActivity.this, getResources().getString(R.string.success));
                    onBackPressed();
                } else {
                    Helper.toast(UpdateProfileActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Citoyen> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(UpdateProfileActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });
    }


    public Citoyen getDataFromInterface() {
        Citoyen citoyen = new Citoyen();
        citoyen.setUsername(editTextNomUser.getText().toString());
        citoyen.setNom(editTextNom.getText().toString());
        citoyen.setPrenom(editTextPrenom.getText().toString());
        citoyen.setEmail(editTextEmail.getText().toString());
        citoyen.setPassword(editTextPassword.getText().toString());
        citoyen.setTelephone(editTextPhone.getText().toString());
        return citoyen;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
    }

}
