package com.skodin.baladity.activities;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.models.Categoriesprobleme;
import com.skodin.baladity.models.Municipalite;
import com.skodin.baladity.models.sugar.Category;
import com.skodin.baladity.services.BaladityAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatPreferenceActivity {
    static SharedPreferences sharedPreferences;
    static List<Categoriesprobleme> categoriesproblemeList;
    static BaladityAPI service;
    static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        activity = this;
        categoriesproblemeList = new ArrayList<>();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        service = ConfService.getRetrofit(SettingsActivity.this).create(BaladityAPI.class);
        setupActionBar();
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            onBackPressed();
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }


    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        // loadHeadersFromResource(R.xml.pref_notification, target);
    }


    protected boolean isValidFragment(String fragmentName) {
        return PrefsFragment.class.getName().equals(fragmentName);
    }

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
            new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object value) {
                    String stringValue = value.toString();

                    if (preference instanceof ListPreference) {
                        ListPreference listPreference = (ListPreference) preference;
                        int index = listPreference.findIndexOfValue(stringValue);

                        preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);

                    } else {
                        preference.setSummary(stringValue);
                    }
                    return true;
                }
            };


    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class PrefsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.prefs);
            setHasOptionsMenu(true);
            ListPreference listPreferenceCategories = (ListPreference) findPreference("key_categorie");
            PreferenceScreen preferenceScreenShare = (PreferenceScreen) findPreference("share");
            PreferenceScreen preferenceScreenEvaluate = (PreferenceScreen) findPreference("evaluate");
            preferenceScreenShare.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + activity.getPackageName());
                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.option)));
                    return false;
                }
            });

            preferenceScreenEvaluate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getPackageName())));
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
                    }
                    return false;
                }
            });

            CharSequence[] entries = new CharSequence[1];
            CharSequence[] entryValues = new CharSequence[1];
            entries[0] = "Tous";
            entryValues[0] = "0";

            listPreferenceCategories.setEntries(entries);
            listPreferenceCategories.setEntryValues(entryValues);


            fillListe(listPreferenceCategories);
        }

        private void fillListe(final ListPreference listPreferenceCategories) {

            //fill liste Catégorie
            Call<List<Categoriesprobleme>> serviceCat = service.getAllCategorieProblemes();
            serviceCat.enqueue(new Callback<List<Categoriesprobleme>>() {
                @Override
                public void onResponse(Call<List<Categoriesprobleme>> call, Response<List<Categoriesprobleme>> response) {
                    List<Categoriesprobleme> listCat = response.body();
                    for (int i = 0; i < listCat.size(); i++) {
                        categoriesproblemeList.add(new Categoriesprobleme(listCat.get(i).getId(), listCat.get(i).getNom()));
                    }

                    CharSequence[] entries1 = new CharSequence[categoriesproblemeList.size() + 1];
                    CharSequence[] entryValues1 = new CharSequence[categoriesproblemeList.size() + 1];
                    entries1[0] = "Tous";
                    entryValues1[0] = "0";
                    for (int i = 0; i < categoriesproblemeList.size(); i++) {
                        entries1[i + 1] = categoriesproblemeList.get(i).getNom();
                        entryValues1[i + 1] = "" + categoriesproblemeList.get(i).getId();
                    }
                    listPreferenceCategories.setEntries(entries1);
                    listPreferenceCategories.setEntryValues(entryValues1);
                    try{
                    listPreferenceCategories.setValueIndex(Integer.parseInt(sharedPreferences.getString("key_categorie", "0")));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<Categoriesprobleme>> call, Throwable t) {
                    t.getMessage();
                }
            });
        }



    }


}
