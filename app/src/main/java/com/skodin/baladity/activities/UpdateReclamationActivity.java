package com.skodin.baladity.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.Switch;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Categoriesprobleme;
import com.skodin.baladity.models.Municipalite;
import com.skodin.baladity.models.Photo;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.ResponseObject;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateReclamationActivity extends AppCompatActivity implements View.OnClickListener {
    @Bind(R.id.editText_description)
    EditText editTextDescription;
    @Bind(R.id.input_description)
    TextInputLayout textInputLayoutDescription;
    @Bind(R.id.flag)
    CheckBox checkBox;
    @Bind(R.id.backgroundImageView)
    ImageView imageViewPhoto1;
    @Bind(R.id.photo2)
    ImageView imageViewPhoto2;
    @Bind(R.id.photo3)
    ImageView imageViewPhoto3;
    @Bind(R.id.photo4)
    ImageView imageViewPhoto4;
    @Bind(R.id.photo5)
    ImageView imageViewPhoto5;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.spinner_categorie)
    MaterialSpinner spinnerCategorie;

    List<Categoriesprobleme> categoriesproblemeList;
    List<String> listeCategorieProbleme;
    int categorieSelected = 0;
    static int click = 1;
    private Uri uri;
    String myObjectBundle;
    Reclamation reclamation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_reclamation);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        fab.setOnClickListener(this);
        imageViewPhoto2.setOnClickListener(this);
        imageViewPhoto3.setOnClickListener(this);
        imageViewPhoto4.setOnClickListener(this);
        imageViewPhoto5.setOnClickListener(this);


        editTextDescription.setScroller(new Scroller(this));
        editTextDescription.setMaxLines(3);
        editTextDescription.setVerticalScrollBarEnabled(true);
        editTextDescription.setMovementMethod(new ScrollingMovementMethod());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            myObjectBundle = extras.getString("reclamation");
        }
        reclamation = new Gson().fromJson(myObjectBundle, Reclamation.class);


        categoriesproblemeList = new ArrayList<>();
        listeCategorieProbleme = new ArrayList<>();


        editTextDescription.setText(reclamation.getDescription());
        checkBox.setChecked(reclamation.getFlag() == 1);

        if (reclamation.getPhotos().size() > 0) {
            setPhotosToImageView(imageViewPhoto1, 0);
        }

        if (reclamation.getPhotos().size() > 1) {
            setPhotosToImageView(imageViewPhoto2, 1);
        } else {
            imageViewPhoto3.setVisibility(View.GONE);
            imageViewPhoto4.setVisibility(View.GONE);
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (reclamation.getPhotos().size() > 2) {
            setPhotosToImageView(imageViewPhoto3, 2);
        } else {
            imageViewPhoto4.setVisibility(View.GONE);
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (reclamation.getPhotos().size() > 3) {
            setPhotosToImageView(imageViewPhoto4, 3);
        } else {
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (reclamation.getPhotos().size() > 4) {
            setPhotosToImageView(imageViewPhoto5, 4);
        }


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reclamation.setFlag(1);
                } else {
                    reclamation.setFlag(0);
                }
            }
        });

        fillSpinner();

    }

    private void setPhotosToImageView(final ImageView imageView, final int pos) {
        try {
            Uri uriPicture = Sdcard.checkExistenceFile(this, Helper.getFileName(ConfService.BASE_URL + reclamation.getPhotos().get(pos).getChemin()), 1);
            //teste si image existe
            if (uriPicture != null) {
                imageView.setImageURI(uriPicture);
            } else {
                // download image and save
                Picasso.with(this).load(ConfService.BASE_URL + reclamation.getPhotos().get(pos).getChemin())
                        .placeholder(R.drawable.carrousel)
                        .error(R.drawable.carrousel)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    imageView.setImageBitmap(bitmap);
                                    Sdcard.savePictures(UpdateReclamationActivity.this, bitmap, Helper.getFileName(ConfService.BASE_URL + reclamation.getPhotos().get(pos).getChemin()), 1);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                imageView.setImageDrawable(errorDrawable);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                imageView.setImageDrawable(placeHolderDrawable);
                            }
                        });
            }
        } catch (Exception e) {
            imageView.setImageResource(R.drawable.carrousel);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void fillSpinner() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<List<Categoriesprobleme>> serviceCat = service.getAllCategorieProblemes();
        serviceCat.enqueue(new Callback<List<Categoriesprobleme>>() {
            @Override
            public void onResponse(Call<List<Categoriesprobleme>> call, Response<List<Categoriesprobleme>> response) {
                List<Categoriesprobleme> listCat = response.body();
                for (int i = 0; i < listCat.size(); i++) {
                    if (reclamation.getIdCategorieProleme() == listCat.get(i).getId()) {
                        categorieSelected = i;
                    }
                    categoriesproblemeList.add(new Categoriesprobleme(listCat.get(i).getId(), listCat.get(i).getNom()));
                    listeCategorieProbleme.add(listCat.get(i).getNom());
                }
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(UpdateReclamationActivity.this, android.R.layout.simple_spinner_item, listeCategorieProbleme);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerCategorie.setAdapter(spinnerAdapter);
                spinnerCategorie.setSelection(categorieSelected + 1);
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Categoriesprobleme>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });


    }


    private Categoriesprobleme getCategorieProbleme(String s) {
        for (int i = 0; i < categoriesproblemeList.size(); i++) {
            if (categoriesproblemeList.get(i).getNom().equals(s)) {
                return categoriesproblemeList.get(i);
            }
        }
        return null;
    }

    @OnClick(R.id.button_update)
    public void update(View view) {
        if (errors()) {
            if (Helper.testConnexionInternet(UpdateReclamationActivity.this)) {
                callServiceExterne();
            } else {
                Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.error_connexion));
            }
        }
    }


    private boolean errors() {
        boolean testDescription = false, testCategorie = false;
        if (!editTextDescription.getText().toString().isEmpty()) {
            testDescription = true;
            textInputLayoutDescription.setErrorEnabled(false);
            editTextDescription.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            if (spinnerCategorie.getSelectedItemId() != 0) {
                testCategorie = true;
                spinnerCategorie.setError(null);
                spinnerCategorie.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            } else {
                spinnerCategorie.setError(getResources().getString(R.string.error_categorie));
                spinnerCategorie.requestFocus();
            }
        } else {
            textInputLayoutDescription.setError(getResources().getString(R.string.error_description));
            editTextDescription.requestFocus();
        }
        return testDescription && testCategorie;
    }


    public void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(UpdateReclamationActivity.this).create(BaladityAPI.class);
        Map<String, String> map = new HashMap<>();
        map.put("degree", String.valueOf(1));
        map.put("description", editTextDescription.getText().toString());
        map.put("flag", String.valueOf(reclamation.getFlag()));
        map.put("idCategorieProleme", "" + getCategorieProbleme(listeCategorieProbleme.get((int) spinnerCategorie.getSelectedItemId() - 1)).getId());
        Call<ResponseObject> callService = service.updateReclamation(reclamation.getId(), map);
        callService.enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {

                    if (response.body().getResult().equals("success")) {
                        Helper.toast(UpdateReclamationActivity.this, UpdateReclamationActivity.this.getResources().getString(R.string.success));
                        finish();
                    } else if (response.body().getResult().equals("impossible")) {
                        Helper.toast(UpdateReclamationActivity.this, UpdateReclamationActivity.this.getResources().getString(R.string.permission_update));
                        finish();
                    }

                } else {
                    Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                click = 1;
                break;
            case R.id.photo2:
                click = 2;
                break;
            case R.id.photo3:
                click = 3;
                break;
            case R.id.photo4:
                click = 4;
                break;
            case R.id.photo5:
                click = 5;
                break;
        }

        if (reclamation.getModifications().size() > 1) {
            Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.permission_update));
        } else {
            takeImage(v);
        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ConfService.REQUEST_IMAGE_CALLERY) {
            if (resultCode == RESULT_OK) {
                uri = data.getData();
            }
        }

        try {

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, option);

            String filename = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            Uri fileUri = Sdcard.saveUploadedPicturesToSdcard(this, bitmap, filename);


            switch (click) {
                case 1:
                    imageViewPhoto1.setImageURI(fileUri);
                    if (reclamation.getPhotos().size() > 0) {
                        callServiceExternePhoto(reclamation.getPhotos().get(0).getId(), new File(fileUri.getPath()));
                    } else {
                        addPhoto(reclamation.getId(), new File(fileUri.getPath()));
                    }
                    break;
                case 2:
                    imageViewPhoto2.setImageURI(fileUri);
                    if (reclamation.getPhotos().size() > 1) {
                        callServiceExternePhoto(reclamation.getPhotos().get(1).getId(), new File(fileUri.getPath()));
                    } else {
                        addPhoto(reclamation.getId(), new File(fileUri.getPath()));
                        imageViewPhoto3.setVisibility(View.VISIBLE);
                    }
                    break;
                case 3:
                    imageViewPhoto3.setImageURI(fileUri);
                    if (reclamation.getPhotos().size() > 2) {
                        callServiceExternePhoto(reclamation.getPhotos().get(2).getId(), new File(fileUri.getPath()));
                    } else {
                        addPhoto(reclamation.getId(), new File(fileUri.getPath()));
                        imageViewPhoto4.setVisibility(View.VISIBLE);
                    }
                    break;
                case 4:
                    imageViewPhoto4.setImageURI(fileUri);
                    if (reclamation.getPhotos().size() > 3) {
                        callServiceExternePhoto(reclamation.getPhotos().get(3).getId(), new File(fileUri.getPath()));
                    } else {
                        addPhoto(reclamation.getId(), new File(fileUri.getPath()));
                        imageViewPhoto5.setVisibility(View.VISIBLE);
                    }
                    break;
                case 5:
                    imageViewPhoto5.setImageURI(fileUri);
                    if (reclamation.getPhotos().size() > 4) {
                        callServiceExternePhoto(reclamation.getPhotos().get(4).getId(), new File(fileUri.getPath()));
                    } else {
                        addPhoto(reclamation.getId(), new File(fileUri.getPath()));
                    }
                    break;

            }
        } catch (Exception e) {
            Helper.toast(this, getResources().getString(R.string.errorOperation));
        }

    }

    public void takeImage(View view) {
        final CharSequence[] items = {getResources().getString(R.string.camera), getResources().getString(R.string.gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.take_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        dialog.dismiss();
                        openCamera();
                        break;
                    case 1:
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, ConfService.REQUEST_IMAGE_CALLERY);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void openCamera() {
        //test of version android
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            // if permission not granted
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                //open Dialog permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA);
                // }

                return;
            }
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void callServiceExternePhoto(int idPhoto, File file) {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(UpdateReclamationActivity.this).create(BaladityAPI.class);
        RequestBody chemin = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody id = ConfService.toRequestBody(String.valueOf(idPhoto));
        Call<Photo> callService = service.updatePhoto(id, chemin);
        callService.enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {

                    Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.success));
                } else {
                    Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });
    }

    public void addPhoto(int idReclamation, File file) {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(UpdateReclamationActivity.this).create(BaladityAPI.class);
        RequestBody chemin = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody id = ConfService.toRequestBody(String.valueOf(idReclamation));
        Call<Photo> callService = service.addPhoto(chemin, id);
        callService.enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {

                    Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.success));
                } else {
                    Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(UpdateReclamationActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
