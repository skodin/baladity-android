package com.skodin.baladity.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarContext;
import com.skodin.baladity.R;
import com.skodin.baladity.adapters.ViewPagerAdapter;
import com.skodin.baladity.fragments.DeclaredClaimFragment;
import com.skodin.baladity.fragments.NotDeclaredClaimFragment;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.ResponseObject;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tabs)
    TabLayout tabLayout;
    @Bind(R.id.viewpager)
    ViewPager viewPager;
    TextView profileEmail, profileName;
    ImageView profileImage, dropDownImage;
    View headerView;
    int i = 0;
    ViewPagerAdapter adapter;
    private boolean backPressedToExitOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        SugarContext.init(this);

        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        // open drawer and select first item
        // drawer.openDrawer(GravityCompat.START);
        navigationView.getMenu().getItem(0).setChecked(true);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        headerView = navigationView.getHeaderView(0);
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toogleHeader();
            }
        });
        setDataToInterface();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                startActivityForResult(new Intent(this, DeclarerActivity.class), 5);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    private void setDataToInterface() {
        profileName = (TextView) headerView.findViewById(R.id.profile_name);
        profileName.setText(Helper.getPreferenceCitoyen(this).getUsername());

        profileEmail = (TextView) headerView.findViewById(R.id.profile_email);
        profileEmail.setText(Helper.getPreferenceCitoyen(this).getEmail());

        dropDownImage = (ImageView) headerView.findViewById(R.id.image_account);
        profileImage = (ImageView) headerView.findViewById(R.id.profile_photo);

        try {
            Uri uriPicture = Sdcard.checkExistenceFile(this, Helper.getFileName(ConfService.BASE_URL + Helper.getPreferenceCitoyen(this).getPhoto()), 0);
            //teste si image existe
            if (uriPicture != null) {
                profileImage.setImageURI(uriPicture);
            } else {
                // download image and save
                Picasso.with(this).load(ConfService.BASE_URL + Helper.getPreferenceCitoyen(this).getPhoto())
                        .placeholder(R.drawable.photo)
                        .error(R.drawable.photo)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    profileImage.setImageBitmap(bitmap);
                                    Sdcard.savePictures(MainActivity.this, bitmap, Helper.getFileName(ConfService.BASE_URL + Helper.getPreferenceCitoyen(MainActivity.this).getPhoto()), 0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                profileImage.setImageDrawable(errorDrawable);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                profileImage.setImageDrawable(placeHolderDrawable);
                            }
                        });
            }
        } catch (Exception e) {
            profileImage.setImageResource(R.drawable.photo);
        }

    }



    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DeclaredClaimFragment(), getResources().getString(R.string.declared));
        adapter.addFragment(new NotDeclaredClaimFragment(), getResources().getString(R.string.not_declared));
        viewPager.setAdapter(adapter);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.item_declarer:
                startActivityForResult(new Intent(this, DeclarerActivity.class), 5);
                break;
            case R.id.nav_my_favories:
                startActivityForResult(new Intent(this, MyFavoriesActivity.class), 5);
                break;
            case R.id.nav_all_claims:
                startActivityForResult(new Intent(this, AllReclamationActivity.class), 5);
                break;
            case R.id.ic_signup:
                toogleHeader();
                if (Helper.testConnexionInternet(this)) {
                    callServiceExterneLogout();
                } else {
                    Helper.toast(this, getResources().getString(R.string.error_connexion));
                }
                break;
            case R.id.ic_profile:
                toogleHeader();
                startActivityForResult(new Intent(this, UpdateProfileActivity.class), 15);
                break;
           /* case R.id.nav_contact:
                startActivityForResult(new Intent(this, ContactActivity.class),5);
                break;
                */
            case R.id.nav_params:
                startActivityForResult(new Intent(this, SettingsActivity.class), 5);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void callServiceExterneLogout() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<ResponseObject> callService = service.logout();
        callService.enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                    Helper.deletePreferenceCitoyen(MainActivity.this);
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                } else {
                    Helper.toast(MainActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(MainActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });

    }


    public void toogleHeader() {
        i++;
        if (i % 2 == 0) {
            dropDownImage.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp);
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer);
            navigationView.getMenu().getItem(0).setChecked(true);
        } else {
            dropDownImage.setImageResource(R.drawable.ic_arrow_drop_up_white_24dp);
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer2);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        navigationView.getMenu().getItem(0).setChecked(true);
        if (requestCode == 15) {
            setDataToInterface();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (backPressedToExitOnce) {
                super.onBackPressed();
                return;
            }

            this.backPressedToExitOnce = true;

            Toast.makeText(this, getResources().getString(R.string.returnExit), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    backPressedToExitOnce = false;
                }
            }, 2000);
        }
    }


}
