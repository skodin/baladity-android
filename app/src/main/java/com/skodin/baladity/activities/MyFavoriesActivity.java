package com.skodin.baladity.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.skodin.baladity.R;
import com.skodin.baladity.adapters.MyFavoriesClaimAdapter;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.models.Favorie;
import com.skodin.baladity.services.BaladityAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFavoriesActivity extends AppCompatActivity {
    @Bind(R.id.recycler_favorie_reclamation)
    RecyclerView recyclerView;
    @Bind(R.id.data)
    RelativeLayout relativeLayout;
    MyFavoriesClaimAdapter adapter;
    List<Favorie> listeFavorie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favories);
        ButterKnife.bind(this);

       try {
           setupActionBar();
           getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       }catch (Exception e){
           e.printStackTrace();
       }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        listeFavorie = new ArrayList<>();

        if (Helper.testConnexionInternet(this)) {
            callServiceExterne();
        } else {
            Helper.toast(this, getResources().getString(R.string.error_connexion));
        }

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(this);
        BaladityAPI service = ConfService.getRetrofit(this).create(BaladityAPI.class);
        Call<List<Favorie>> callService = service.getFavories(Helper.getPreferenceCitoyen(MyFavoriesActivity.this).getId());
        callService.enqueue(new Callback<List<Favorie>>() {
            @Override
            public void onResponse(Call<List<Favorie>> call, Response<List<Favorie>> response) {
                if (response.isSuccess()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        Favorie favorie = new Favorie();
                        favorie.setId(response.body().get(i).getId());
                        favorie.setIdCitoyen(response.body().get(i).getIdCitoyen());
                        favorie.setIdReclamation(response.body().get(i).getIdReclamation());
                        favorie.setProbleme(response.body().get(i).getProbleme());
                        listeFavorie.add(favorie);
                    }
                    adapter = new MyFavoriesClaimAdapter(MyFavoriesActivity.this, listeFavorie,relativeLayout);
                    recyclerView.setAdapter(adapter);

                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if(listeFavorie.size() == 0){
                        relativeLayout.setVisibility(View.VISIBLE);
                    }else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                } else {
                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    if(listeFavorie.size() == 0){
                        relativeLayout.setVisibility(View.VISIBLE);
                    }else {
                        relativeLayout.setVisibility(View.GONE);
                    }
                    Helper.toast(MyFavoriesActivity.this, getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<List<Favorie>> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if(listeFavorie.size() == 0){
                    relativeLayout.setVisibility(View.VISIBLE);
                }else {
                    relativeLayout.setVisibility(View.GONE);
                }
                Helper.toast(MyFavoriesActivity.this, getResources().getString(R.string.problem_connexion));

            }
        });



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
