package com.skodin.baladity.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.sugar.Category;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.models.sugar.Municipality;
import com.skodin.baladity.models.sugar.Picture;
import com.skodin.baladity.services.ServiceInterne;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;

public class UpdateNonDeclarerActivity extends AppCompatActivity implements View.OnClickListener {
    @Bind(R.id.editText_description)
    EditText editTextDescription;
    @Bind(R.id.input_description)
    TextInputLayout textInputLayoutDescription;
    @Bind(R.id.flag)
    CheckBox checkBox;
    @Bind(R.id.spinner_categorie)
    MaterialSpinner spinnerCategorie;
    @Bind(R.id.photo1)
    ImageView imageViewPhoto1;
    @Bind(R.id.photo2)
    ImageView imageViewPhoto2;
    @Bind(R.id.photo3)
    ImageView imageViewPhoto3;
    @Bind(R.id.photo4)
    ImageView imageViewPhoto4;
    @Bind(R.id.photo5)
    ImageView imageViewPhoto5;

    List<Municipality> municipaliteList;
    List<Category> categoriesproblemeList;
    List<String> listeMunicipalites, listeCategorieProbleme;


    String myObjectBundle;
    private Uri file1Uri, file2Uri, file3Uri, file4Uri, file5Uri;
    int categorieSelected = 0;
    Claim claim;
    int i = 0;
    static int click = 1;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_non_declarer);
        ButterKnife.bind(this);
        setupActionBar();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            myObjectBundle = extras.getString("claim");
        }
        claim = new Gson().fromJson(myObjectBundle, Claim.class);
        categoriesproblemeList = new ArrayList<>();
        municipaliteList = new ArrayList<>();
        listeMunicipalites = new ArrayList<>();
        listeCategorieProbleme = new ArrayList<>();
        editTextDescription.setText(claim.getDescription());
        checkBox.setChecked(claim.getFlag() == 1);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    claim.setFlag(1);
                } else {
                    claim.setFlag(0);
                }
            }
        });

        imageViewPhoto1.setOnClickListener(this);
        imageViewPhoto2.setOnClickListener(this);
        imageViewPhoto3.setOnClickListener(this);
        imageViewPhoto4.setOnClickListener(this);
        imageViewPhoto5.setOnClickListener(this);

        if (claim.getPictures().size() > 0) {
            setPhotosToImageView(imageViewPhoto1, 0);
        }else {
            imageViewPhoto2.setVisibility(View.GONE);
            imageViewPhoto3.setVisibility(View.GONE);
            imageViewPhoto4.setVisibility(View.GONE);
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (claim.getPictures().size() > 1) {
            setPhotosToImageView(imageViewPhoto2, 1);
        }else {
            imageViewPhoto3.setVisibility(View.GONE);
            imageViewPhoto4.setVisibility(View.GONE);
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (claim.getPictures().size() > 2) {
            setPhotosToImageView(imageViewPhoto3, 2);
        }else {
            imageViewPhoto4.setVisibility(View.GONE);
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (claim.getPictures().size() > 3) {
            setPhotosToImageView(imageViewPhoto4, 3);
        }else {
            imageViewPhoto5.setVisibility(View.GONE);
        }

        if (claim.getPictures().size() > 4) {
            setPhotosToImageView(imageViewPhoto5, 4);
        }


        //  ImageAdapter adapter = new ImageAdapter(this, claim.getPictures());
      //  viewPager.setAdapter(adapter);

        fillSpinner();

        try {
            file1Uri = Uri.parse(claim.getPictures().get(0).getChemin());
            file2Uri = Uri.parse(claim.getPictures().get(1).getChemin());
            file3Uri = Uri.parse(claim.getPictures().get(2).getChemin());
            file4Uri = Uri.parse(claim.getPictures().get(3).getChemin());
            file5Uri = Uri.parse(claim.getPictures().get(4).getChemin());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setPhotosToImageView(ImageView imageViewPhoto, int i) {
        imageViewPhoto.setImageURI(Uri.parse(claim.getPictures().get(i).getChemin()));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    private void fillSpinner() {
        categoriesproblemeList = Category.listAll(Category.class);
        for (int i = 0; i < categoriesproblemeList.size(); i++) {
            if (claim.getIdCategorieProleme() == categoriesproblemeList.get(i).getIdCategory()) {
                categorieSelected = i;
            }
            listeCategorieProbleme.add(categoriesproblemeList.get(i).getNom());
        }
        ArrayAdapter<String> spinnerAdapter1 = new ArrayAdapter<String>(UpdateNonDeclarerActivity.this, android.R.layout.simple_spinner_item, listeCategorieProbleme);
        spinnerAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategorie.setAdapter(spinnerAdapter1);
        spinnerCategorie.setSelection(categorieSelected + 1);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    @OnClick(R.id.button_update)
    public void update(View view) {
        if (errors()) {
            claim.setIdCategorieProleme(getCategorieProbleme(listeCategorieProbleme.get((int) spinnerCategorie.getSelectedItemId() - 1)).getIdCategory());
            claim.setDescription(editTextDescription.getText().toString());
            ServiceInterne.updateClaim(claim);
            Helper.toast(this, getResources().getString(R.string.success));
            onBackPressed();
        }
    }




    private boolean errors() {
        boolean testDescription = false, testCategorie = false;
        if (!editTextDescription.getText().toString().isEmpty()) {
            testDescription = true;
            textInputLayoutDescription.setErrorEnabled(false);
            editTextDescription.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            if (spinnerCategorie.getSelectedItemId() != 0) {
                testCategorie = true;
                spinnerCategorie.setError(null);
                spinnerCategorie.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            } else {
                spinnerCategorie.setError(getResources().getString(R.string.error_categorie));
                spinnerCategorie.requestFocus();
            }
        } else {
            textInputLayoutDescription.setError(getResources().getString(R.string.error_description));
            editTextDescription.requestFocus();
        }
        return testDescription && testCategorie ;
    }


    @OnClick(R.id.button_delete)
    public void delete(View view) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.confirmation));
        dialog.setMessage(getResources().getString(R.string.delete_claim));
        dialog.setPositiveButton(getResources().getString(R.string.confirmer), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ServiceInterne.delete(claim.getId());
                deleteMyPictures();
                onBackPressed();
            }
        });
        dialog.setNegativeButton(getResources().getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog myDialog = dialog.create();
        myDialog.show();
        Button buttonNegative = myDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button buttonPositive = myDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonNegative.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        buttonPositive.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }



    private Municipality getMunicipalite(String s) {
        for (int i = 0; i < municipaliteList.size(); i++) {
            if (municipaliteList.get(i).getNom().equals(s)) {
                return municipaliteList.get(i);
            }
        }
        return null;
    }

    private Category getCategorieProbleme(String s) {
        for (int i = 0; i < categoriesproblemeList.size(); i++) {
            if (categoriesproblemeList.get(i).getNom().equals(s)) {
                return categoriesproblemeList.get(i);
            }
        }
        return null;
    }


    private void deleteMyPictures() {
        if (file1Uri != null) {
            Sdcard.deletePictureFromStorage(file1Uri);
        }
        if (file2Uri != null) {
            Sdcard.deletePictureFromStorage(file2Uri);
        }
        if (file3Uri != null) {
            Sdcard.deletePictureFromStorage(file3Uri);
        }
        if (file4Uri != null) {
            Sdcard.deletePictureFromStorage(file4Uri);
        }
        if (file5Uri != null) {
            Sdcard.deletePictureFromStorage(file5Uri);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo1:
                click = 1;
                break;
            case R.id.photo2:
                click = 2;
                break;
            case R.id.photo3:
                click = 3;
                break;
            case R.id.photo4:
                click = 4;
                break;
            case R.id.photo5:
                click = 5;
                break;
        }
        takeImage(v);
    }


    public void takeImage(View view) {
        final CharSequence[] items = {getResources().getString(R.string.camera),getResources().getString(R.string.gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.take_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        dialog.dismiss();
                        openCamera();
                        break;
                    case 1:
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, ConfService.REQUEST_IMAGE_CALLERY);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void openCamera() {
        //test of version android
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1){
            // if permission not granted
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                //open Dialog permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA);
                // }

                return;
            }
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case DeclarerActivity.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = Uri.fromFile(Sdcard.getOutputMediaFile(this));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, ConfService.REQUEST_IMAGE_CAMERA);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == ConfService.REQUEST_IMAGE_CALLERY){
            if (resultCode == RESULT_OK) {
                uri=data.getData();
            }
        }

        try {

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, option);

            String filename = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
           Uri fileUri = Sdcard.saveUploadedPicturesToSdcard(this,bitmap, filename);

            switch (click) {
                case 1:
                    imageViewPhoto1.setImageURI(fileUri);
                    if (claim.getPictures().size() > 0) {
                        ServiceInterne.updatePicture(claim.getPictures().get(0),fileUri.toString());
                    }else {
                        fileUri = Sdcard.movefile(fileUri, this);
                        ServiceInterne.savePicture(new Picture(fileUri.toString(), claim));
                        imageViewPhoto2.setVisibility(View.VISIBLE);
                    }
                    break;
                case 2:
                    imageViewPhoto2.setImageURI(fileUri);
                    if(claim.getPictures().size() > 1){
                        ServiceInterne.updatePicture(claim.getPictures().get(1), fileUri.toString());
                    }else {
                        fileUri = Sdcard.movefile(fileUri, this);
                        ServiceInterne.savePicture(new Picture(fileUri.toString(), claim));
                        imageViewPhoto3.setVisibility(View.VISIBLE);
                    }
                    break;
                case 3:
                    imageViewPhoto3.setImageURI(fileUri);
                    if(claim.getPictures().size() > 2){
                        ServiceInterne.updatePicture(claim.getPictures().get(2), fileUri.toString());
                    }else {
                        fileUri = Sdcard.movefile(fileUri, this);
                        ServiceInterne.savePicture(new Picture(fileUri.toString(), claim));
                        imageViewPhoto4.setVisibility(View.VISIBLE);
                    }
                    break;
                case 4:
                    imageViewPhoto4.setImageURI(fileUri);
                    if(claim.getPictures().size() > 3){
                        ServiceInterne.updatePicture(claim.getPictures().get(3), fileUri.toString());
                    }else {
                        fileUri = Sdcard.movefile(fileUri, this);
                        ServiceInterne.savePicture(new Picture(fileUri.toString(), claim));
                        imageViewPhoto5.setVisibility(View.VISIBLE);
                    }
                    break;
                case 5:
                    imageViewPhoto5.setImageURI(fileUri);
                    if(claim.getPictures().size() > 4){
                        ServiceInterne.updatePicture(claim.getPictures().get(4), fileUri.toString());
                    }else {
                        fileUri = Sdcard.movefile(fileUri, this);
                        ServiceInterne.savePicture(new Picture(fileUri.toString(), claim));

                    }
                    break;
            }
        } catch (Exception e) {
            Helper.toast(this, getResources().getString(R.string.errorOperation));
        }



    }

}
