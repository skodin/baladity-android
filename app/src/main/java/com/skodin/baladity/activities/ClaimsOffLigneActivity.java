package com.skodin.baladity.activities;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.orm.SugarContext;
import com.skodin.baladity.R;
import com.skodin.baladity.adapters.NotDeclaredClaimAdapter;
import com.skodin.baladity.adapters.OffLigneClaimAdapter;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.services.ServiceInterne;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClaimsOffLigneActivity extends AppCompatActivity {
    @Bind(R.id.recycler_non_declarer)
    RecyclerView recyclerView;
    @Bind(R.id.data)
    RelativeLayout relativeLayout;

    OffLigneClaimAdapter adapter;
    List<Claim> claimList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claims_off_ligne);
        ButterKnife.bind(this);
        SugarContext.init(this);

        setupActionBar();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        claimList = new ArrayList<>();
        adapter = new OffLigneClaimAdapter(this, claimList,relativeLayout);
        callServiceInterne();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
                return true;
            case R.id.action_add:
                startActivityForResult(new Intent(this, DeclarerActivity.class), 5);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.off_ligne_main, menu);
        return super.onCreateOptionsMenu(menu);
    }



    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void callServiceInterne() {
        claimList.clear();
        adapter.notifyDataSetChanged();

        for (int i = 0; i < ServiceInterne.getAllClaim().size(); i++) {
            claimList.add(ServiceInterne.getAllClaim().get(i));
        }
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if(claimList.size() == 0){
            relativeLayout.setVisibility(View.VISIBLE);

        }else {
            relativeLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 5){
            callServiceInterne();
        }
    }
}
