package com.skodin.baladity.activities;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.models.Contact;
import com.skodin.baladity.services.BaladityAPI;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactActivity extends AppCompatActivity {

    @Bind(R.id.editText_subject)
    EditText editTextSubject;
    @Bind(R.id.editText_msg)
    EditText editTextMessage;
    @Bind(R.id.input_subject)
    TextInputLayout textInputLayoutSubject;
    @Bind(R.id.input_message)
    TextInputLayout textInputLayoutMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);

        setupActionBar();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_send)
    public void sendMail(View view) {
        if (errors()) {
            if (Helper.testConnexionInternet(ContactActivity.this)) {
                callServiceExterne();
            } else {
                Helper.toast(this, getResources().getString(R.string.error_connexion));
            }
        }
    }

    public Boolean errors() {
        boolean testSubject = false, testMessage = false;
        if (!editTextSubject.getText().toString().isEmpty()) {
            testSubject = true;
            textInputLayoutSubject.setErrorEnabled(false);
            editTextSubject.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            if (!editTextMessage.getText().toString().isEmpty()) {
                testMessage = true;
                textInputLayoutMessage.setErrorEnabled(false);
                editTextMessage.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP);
            } else {
                textInputLayoutMessage.setError(getResources().getString(R.string.error_message));
                editTextMessage.requestFocus();
            }
        } else {
            textInputLayoutSubject.setError(getResources().getString(R.string.error_sujet));
            editTextSubject.requestFocus();
        }
        return testSubject && testMessage;
    }

    private void callServiceExterne() {
        final ProgressDialog mProgressDialog = Helper.showProgress(ContactActivity.this);
        BaladityAPI service = ConfService.getRetrofit(ContactActivity.this).create(BaladityAPI.class);
        Call<Contact> callService = service.sendMail(getDataFromInterface());
        callService.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                    Helper.toast(ContactActivity.this, getResources().getString(R.string.success));
                    editTextSubject.setText("");
                    editTextMessage.setText("");
                } else {
                    Helper.toast(ContactActivity.this, getResources().getString(R.string.errorOperation));
                }
            }
            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(ContactActivity.this, getResources().getString(R.string.problem_connexion));
            }
        });

    }

    private Contact getDataFromInterface() {
        Contact contact = new Contact();
        contact.setSujet(editTextSubject.getText().toString());
        contact.setMessage(editTextMessage.getText().toString());
        contact.setEmail(Helper.getPreferenceCitoyen(ContactActivity.this).getEmail());
        return contact;
    }


}
