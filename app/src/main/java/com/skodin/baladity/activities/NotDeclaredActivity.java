package com.skodin.baladity.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.orm.SugarContext;
import com.skodin.baladity.R;
import com.skodin.baladity.adapters.OffLigneClaimAdapter;
import com.skodin.baladity.helpers.Helper;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotDeclaredActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_declared);
        ButterKnife.bind(this);
        SugarContext.init(this);
    }


    @OnClick(R.id.fab)
    public void fabPlus(View view) {
        if (Helper.testConnexionInternet(this)) {
            if (Helper.getPreferenceCitoyen(this) != null) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
        } else {
            startActivity(new Intent(this, ClaimsOffLigneActivity.class));
        }

    }

    @OnClick(R.id.btn_ressayer)
    public void tryAgain(View view) {
        if (Helper.testConnexionInternet(this)) {
            if (Helper.getPreferenceCitoyen(this) != null) {
                startActivity(new Intent(this, MainActivity.class));
            } else {
                startActivity(new Intent(this, LoginActivity.class));
            }
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
