package com.skodin.baladity.models;


public class Favorie {

    int id;
    int idReclamation;
    int idCitoyen;
    Reclamation probleme;

    public Reclamation getProbleme() {
        return probleme;
    }

    public Favorie() {
    }

    public void setProbleme(Reclamation probleme) {
        this.probleme = probleme;
    }

    public Favorie(int idReclamation, int idCitoyen) {
        this.idReclamation = idReclamation;
        this.idCitoyen = idCitoyen;
    }

    public int getIdReclamation() {
        return idReclamation;
    }

    public void setIdReclamation(int idReclamation) {
        this.idReclamation = idReclamation;
    }

    public int getIdCitoyen() {
        return idCitoyen;
    }

    public void setIdCitoyen(int idCitoyen) {
        this.idCitoyen = idCitoyen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
