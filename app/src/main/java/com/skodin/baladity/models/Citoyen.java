package com.skodin.baladity.models;


public class Citoyen {

    String username;
    String nom;
    String prenom;
    String email;
    String password;
    String telephone;
    String photo;
    String token;
    int estConfirmer;
    int estBlocker;
    int id;
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getEstConfirmer() {
        return estConfirmer;
    }

    public void setEstConfirmer(int estConfirmer) {
        this.estConfirmer = estConfirmer;
    }

    public int getEstBlocker() {
        return estBlocker;
    }

    public void setEstBlocker(int estBlocker) {
        this.estBlocker = estBlocker;
    }


}
