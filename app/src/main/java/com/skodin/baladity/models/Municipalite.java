package com.skodin.baladity.models;


public class Municipalite {

    int id;
    String nom;
    int idGouvernorat;

    public Municipalite(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdGouvernorat() {
        return idGouvernorat;
    }

    public void setIdGouvernorat(int idGouvernorat) {
        this.idGouvernorat = idGouvernorat;
    }
}
