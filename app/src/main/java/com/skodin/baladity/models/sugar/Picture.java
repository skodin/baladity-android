package com.skodin.baladity.models.sugar;


import com.orm.SugarRecord;

public class Picture extends SugarRecord {
    String chemin;
    Claim claim;

    public Picture() {
    }

    public Picture(String chemin, Claim claim) {
        this.chemin = chemin;
        this.claim=claim;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }



    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }


}
