package com.skodin.baladity.models.sugar;

import com.orm.SugarRecord;

public class Municipality extends SugarRecord {

    int idMunicipality;
    String nom;


    public int getIdMunicipality() {
        return idMunicipality;
    }

    public void setIdMunicipality(int idMunicipality) {
        this.idMunicipality = idMunicipality;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
