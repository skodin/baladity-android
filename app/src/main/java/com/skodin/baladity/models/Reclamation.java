package com.skodin.baladity.models;


import java.util.List;

public class Reclamation {

    int degree;
    String description;
    int id;
    int flag;
    Double longitude;
    Double latitude;
    List<Photo> photos;
    List<Modification> modifications;
    Citoyen citoyen;
    int idMunicipalite;
    int idCategorieProleme;
    Municipalite delegation;
    Categoriesprobleme categorieproleme;


    public Categoriesprobleme getCategorieproleme() {
        return categorieproleme;
    }

    public void setCategorieproleme(Categoriesprobleme categorieproleme) {
        this.categorieproleme = categorieproleme;
    }

    public Municipalite getDelegation() {
        return delegation;
    }

    public void setDelegation(Municipalite delegation) {
        this.delegation = delegation;
    }

    public Citoyen getCitoyen() {
        return citoyen;
    }

    public void setCitoyen(Citoyen citoyen) {
        this.citoyen = citoyen;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getIdMunicipalite() {
        return idMunicipalite;
    }

    public void setIdMunicipalite(int idMunicipalite) {
        this.idMunicipalite = idMunicipalite;
    }

    public int getIdCategorieProleme() {
        return idCategorieProleme;
    }

    public void setIdCategorieProleme(int idCategorieProleme) {
        this.idCategorieProleme = idCategorieProleme;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<Modification> getModifications() {
        return modifications;
    }

    public void setModifications(List<Modification> modifications) {
        this.modifications = modifications;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
