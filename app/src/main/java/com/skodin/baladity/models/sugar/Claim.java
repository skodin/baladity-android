package com.skodin.baladity.models.sugar;

import com.orm.SugarRecord;

import java.util.List;

public class Claim extends SugarRecord {
    int degree;
    String description;
    int flag;
    Double longitude;
    Double latitude;
    int idMunicipalite;
    int idCategorieProleme;


    public List<Picture> getPictures() {
        return Picture.find(Picture.class, "claim = ?", String.valueOf(getId()));
    }

    public Claim() {
    }

    public int getIdMunicipalite() {
        return idMunicipalite;
    }

    public void setIdMunicipalite(int idMunicipalite) {
        this.idMunicipalite = idMunicipalite;
    }

    public int getIdCategorieProleme() {
        return idCategorieProleme;
    }

    public void setIdCategorieProleme(int idCategorieProleme) {
        this.idCategorieProleme = idCategorieProleme;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


}
