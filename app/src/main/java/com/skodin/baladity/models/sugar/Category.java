package com.skodin.baladity.models.sugar;

import com.orm.SugarRecord;


public class Category extends SugarRecord {

    int idCategory;
    String nom;


    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}