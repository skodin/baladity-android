package com.skodin.baladity.models;

import com.skodin.baladity.helpers.ConfService;
public class Photo {
    int id;
    String chemin;
    int idReclamation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = ConfService.BASE_URL+chemin;
    }

    public int getIdReclamation() {
        return idReclamation;
    }

    public void setIdReclamation(int idReclamation) {
        this.idReclamation = idReclamation;
    }
}
