package com.skodin.baladity.models;


public class ResponseObject {

    String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
