package com.skodin.baladity.models;

public class Semblable {
    int id;
    int idReclamation1;
    int idReclamation2;
    int idUser;
    String message;

    public Semblable(int idReclamation1, int idReclamation2, int idUser, String message) {
        this.idReclamation1 = idReclamation1;
        this.idReclamation2 = idReclamation2;
        this.idUser = idUser;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdReclamation1() {
        return idReclamation1;
    }

    public void setIdReclamation1(int idReclamation1) {
        this.idReclamation1 = idReclamation1;
    }

    public int getIdREclamation2() {
        return idReclamation2;
    }

    public void setIdREclamation2(int idREclamation2) {
        this.idReclamation2 = idREclamation2;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
