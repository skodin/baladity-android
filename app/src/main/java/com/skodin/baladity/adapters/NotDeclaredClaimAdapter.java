package com.skodin.baladity.adapters;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.DeclarerActivity;
import com.skodin.baladity.activities.MapsActivity;
import com.skodin.baladity.activities.UpdateNonDeclarerActivity;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.services.BaladityAPI;
import com.skodin.baladity.services.ServiceInterne;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotDeclaredClaimAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Claim> data = new ArrayList<>();
    Activity activity;
    Button btnDeclareAll;
    RelativeLayout relativeLayout;


    public NotDeclaredClaimAdapter(Activity activity, List<Claim> data, Button button, RelativeLayout relativeLayout) {
        this.activity = activity;
        this.data = data;
        this.btnDeclareAll = button;
        this.relativeLayout = relativeLayout;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public Claim getItem(int position) {
        return data.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_non_declarer_reclamation, parent, false);
        return new NotDeclaredClaimViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final NotDeclaredClaimViewHolder notDeclaredClaimViewHolder = (NotDeclaredClaimViewHolder) holder;
        final Claim claim = data.get(position);
        notDeclaredClaimViewHolder.photo1.setImageURI(Uri.parse(claim.getPictures().get(0).getChemin()));

        notDeclaredClaimViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteClaim(claim, position);
            }
        });

        notDeclaredClaimViewHolder.delete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_delete);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

        notDeclaredClaimViewHolder.send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callServiceExterne(claim, position);
            }
        });

        notDeclaredClaimViewHolder.send.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_declarer);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

    }


    public class NotDeclaredClaimViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.photo)
        ImageView photo1;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.send)
        ImageView send;

        public NotDeclaredClaimViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public void deleteClaim(final Claim claim, final int position) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(activity.getResources().getString(R.string.confirmation));
        dialog.setMessage(activity.getResources().getString(R.string.delete_claim));
        dialog.setPositiveButton(activity.getResources().getString(R.string.confirmer), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ServiceInterne.delete(claim.getId());
                try {
                    for (int j = 0; j < 5; j++) {
                        Sdcard.deletePictureFromStorage(Uri.parse(claim.getPictures().get(j).getChemin()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notiferAdapter(position);
                Helper.toast(activity, activity.getResources().getString(R.string.success));
            }
        });
        dialog.setNegativeButton(activity.getResources().getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog myDialog = dialog.create();
        myDialog.show();
        Button buttonNegative = myDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button buttonPositive = myDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonNegative.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        buttonPositive.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public void callServiceExterne(final Claim claim, final int position) {
        final ProgressDialog mProgressDialog = Helper.showProgress(activity);
        BaladityAPI service = ConfService.getRetrofit(activity).create(BaladityAPI.class);
        Map<String, RequestBody> map = new HashMap<>();
        try {
            if (claim.getPictures().get(0).getChemin() != null) {
                Uri file1Uri = Uri.parse(claim.getPictures().get(0).getChemin());
                map.put("photo1\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file1Uri.getPath())));
            }
            if (claim.getPictures().get(1).getChemin() != null) {
                Uri file2Uri = Uri.parse(claim.getPictures().get(1).getChemin());
                map.put("photo2\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file2Uri.getPath())));
            }
            if (claim.getPictures().get(2).getChemin() != null) {
                Uri file3Uri = Uri.parse(claim.getPictures().get(2).getChemin());
                map.put("photo3\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file3Uri.getPath())));
            }
            if (claim.getPictures().get(3).getChemin() != null) {
                Uri file4Uri = Uri.parse(claim.getPictures().get(3).getChemin());
                map.put("photo4\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file4Uri.getPath())));
            }
            if (claim.getPictures().get(4).getChemin() != null) {
                Uri file5Uri = Uri.parse(claim.getPictures().get(4).getChemin());
                map.put("photo5\"; filename=\"pp.png\"", ConfService.toRequestBodyFile(new File(file5Uri.getPath())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("degree", ConfService.toRequestBody(String.valueOf(claim.getDegree())));
        map.put("description", ConfService.toRequestBody(claim.getDescription()));
        map.put("longitude", ConfService.toRequestBody(String.valueOf(claim.getLongitude())));
        map.put("latitude", ConfService.toRequestBody(String.valueOf(claim.getLatitude())));
        map.put("flag", ConfService.toRequestBody(String.valueOf(claim.getFlag())));
        map.put("idCitoyen", ConfService.toRequestBody(String.valueOf(Helper.getPreferenceCitoyen(activity).getId())));
        map.put("idMunicipalite", ConfService.toRequestBody("" + claim.getIdMunicipalite()));
        map.put("idCategorieProleme", ConfService.toRequestBody("" + claim.getIdCategorieProleme()));
        Call<Reclamation> callService = service.declarer(map);
        callService.enqueue(new Callback<Reclamation>() {
            @Override
            public void onResponse(Call<Reclamation> call, Response<Reclamation> response) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if (response.isSuccess()) {
                    ServiceInterne.delete(claim.getId());
                    try {
                        for (int j = 0; j < 5; j++) {
                            Sdcard.deletePictureFromStorage(Uri.parse(claim.getPictures().get(j).getChemin()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Helper.toast(activity, activity.getResources().getString(R.string.success));
                    notiferAdapter(position);
                } else {
                    Helper.toast(activity, activity.getResources().getString(R.string.errorOperation));
                }
            }

            @Override
            public void onFailure(Call<Reclamation> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Helper.toast(activity, activity.getResources().getString(R.string.problem_connexion));
            }
        });

    }


    public void notiferAdapter(int position) {
        data.remove(position);
        notifyDataSetChanged();
        if (data.size() == 0) {
            btnDeclareAll.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
        } else {
            relativeLayout.setVisibility(View.GONE);
            btnDeclareAll.setVisibility(View.VISIBLE);
        }
    }


}
