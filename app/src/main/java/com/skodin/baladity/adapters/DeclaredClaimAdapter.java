package com.skodin.baladity.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.CommentActivity;
import com.skodin.baladity.activities.DetailActivity;
import com.skodin.baladity.activities.MapsActivity;
import com.skodin.baladity.activities.UpdateReclamationActivity;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.ResponseObject;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DeclaredClaimAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Reclamation> data = new ArrayList<>();
    Activity activity;
    RelativeLayout relativeLayout;
    final int VIEW_ITEM = 1;
    final int VIEW_PROG = 0;

    int visibleThreshold = 1;
    int lastVisibleItem, totalItemCount;
    boolean loading;
    OnLoadMoreListener onLoadMoreListener;


    public DeclaredClaimAdapter(Activity activity, List<Reclamation> data, RecyclerView recyclerView,RelativeLayout relativeLayout) {
        this.activity = activity;
        this.data = data;
        this.relativeLayout=relativeLayout;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_reclamation, parent, false);
            vh = new DeclaredClaimViewHolder(view);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_progress_bar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof DeclaredClaimViewHolder) {
            final DeclaredClaimViewHolder declaredClaimViewHolder = (DeclaredClaimViewHolder) holder;
            final Reclamation reclamation = data.get(position);
            declaredClaimViewHolder.textViewDate.setText(reclamation.getModifications().get(0).getDate().substring(0, 10));

            try {
                Uri uriPicture = Sdcard.checkExistenceFile(activity,Helper.getFileName(reclamation.getPhotos().get(0).getChemin()), 1);
                //teste si image existe
                if (uriPicture != null) {
                    declaredClaimViewHolder.photo1.setImageURI(uriPicture);
                } else {
                    // download image and save
                    Picasso.with(activity).load(ConfService.BASE_URL + reclamation.getPhotos().get(0).getChemin())
                            .placeholder(R.drawable.carrousel)
                            .error(R.drawable.carrousel)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                    try {
                                        declaredClaimViewHolder.photo1.setImageBitmap(bitmap);
                                        Sdcard.savePictures(activity,bitmap, Helper.getFileName(reclamation.getPhotos().get(0).getChemin()), 1);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {
                                    declaredClaimViewHolder.photo1.setImageDrawable(errorDrawable);
                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {
                                    declaredClaimViewHolder.photo1.setImageDrawable(placeHolderDrawable);
                                }
                            });
                }
            } catch (Exception e) {
                declaredClaimViewHolder.photo1.setImageResource(R.drawable.carrousel);
            }

            if(reclamation.getLatitude() != 0  && reclamation.getLongitude() != 0){
                declaredClaimViewHolder.maps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, MapsActivity.class);
                        Bundle b = new Bundle();
                        b.putDouble("latitude", reclamation.getLatitude());
                        b.putDouble("longitude", reclamation.getLongitude());
                        intent.putExtras(b);
                        activity.startActivity(intent);
                    }
                });
            }else {
                declaredClaimViewHolder.maps.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_map_marker_grey600_24dp));
            }




            declaredClaimViewHolder.maps.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_maps);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity, contentDesc, pos);
                    return true;
                }
            });

            declaredClaimViewHolder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, DetailActivity.class);
                    intent.putExtra("reclamation", new Gson().toJson(reclamation));
                    activity.startActivityForResult(intent, 5);
                }
            });

            declaredClaimViewHolder.more.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_détail);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity, contentDesc, pos);
                    return true;
                }
            });

            declaredClaimViewHolder.photo1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, DetailActivity.class);
                    intent.putExtra("reclamation", new Gson().toJson(reclamation));
                    activity.startActivity(intent);
                }
            });


            declaredClaimViewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAlert(reclamation.getId(), position);

                }
            });
            declaredClaimViewHolder.delete.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_delete);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity, contentDesc, pos);
                    return true;
                }
            });

            declaredClaimViewHolder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, UpdateReclamationActivity.class);
                    intent.putExtra("reclamation", new Gson().toJson(reclamation));
                    activity.startActivityForResult(intent, 5);
                }
            });

            declaredClaimViewHolder.update.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_update);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity, contentDesc, pos);
                    return true;
                }
            });

            declaredClaimViewHolder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, CommentActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("idReclamation", reclamation.getId());
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            });

            declaredClaimViewHolder.comment.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_commentaire);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity, contentDesc, pos);
                    return true;
                }
            });

        } else {
            final ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progressBar1)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    private void showAlert(final int id, final int postion) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(activity.getResources().getString(R.string.confirmation));
        dialog.setMessage(activity.getResources().getString(R.string.delete_claim));
        dialog.setPositiveButton(activity.getResources().getString(R.string.confirmer), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final ProgressDialog mProgressDialog = Helper.showProgress(activity);
                BaladityAPI service = ConfService.getRetrofit(activity).create(BaladityAPI.class);
                Call<ResponseObject> callService = service.deleteReclamation(id);
                callService.enqueue(new Callback<ResponseObject>() {
                    @Override
                    public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        if (response.isSuccess()) {
                            try {
                                if (response.body().getResult().equals("success")) {
                                    Helper.toast(activity, activity.getResources().getString(R.string.success));
                                    data.remove(postion);
                                    notifyDataSetChanged();
                                    if (data.size() == 0) {
                                        relativeLayout.setVisibility(View.VISIBLE);
                                    } else {
                                        relativeLayout.setVisibility(View.GONE);
                                    }
                                } else if (response.body().getResult().equals("impossible")) {
                                    Helper.toast(activity, activity.getResources().getString(R.string.permission_delete));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Helper.toast(activity, activity.getResources().getString(R.string.errorOperation));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseObject> call, Throwable t) {
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Helper.toast(activity, activity.getResources().getString(R.string.errorOperation));
                    }
                });
            }
        });
        dialog.setNegativeButton(activity.getResources().getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog myDialog = dialog.create();
        myDialog.show();
        Button buttonNegative = myDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button buttonPositive = myDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonNegative.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        buttonPositive.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }


    public class DeclaredClaimViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.date)
        TextView textViewDate;

        @Bind(R.id.photo)
        ImageView photo1;

        @Bind(R.id.maps)
        ImageView maps;

        @Bind(R.id.detail)
        ImageView more;

        @Bind(R.id.delete)
        ImageView delete;

        @Bind(R.id.update)
        ImageView update;
        @Bind(R.id.comment)
        ImageView comment;

        public DeclaredClaimViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
