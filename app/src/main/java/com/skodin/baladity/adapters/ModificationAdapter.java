package com.skodin.baladity.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skodin.baladity.R;
import com.skodin.baladity.activities.DetailActivity;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.models.Modification;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ModificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Modification> data = new ArrayList<>();
    DetailActivity activity;


    public ModificationAdapter(DetailActivity activity, List<Modification> data) {
        this.activity = activity;
        this.data = data;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public Modification getItem(int position) {
        return data.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_modification, parent, false);
        return new ModificationViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final ModificationViewHolder modificationViewHolder = (ModificationViewHolder) holder;
        final Modification modification = data.get(position);
        modificationViewHolder.textViewDate.setText(modification.getDate().substring(0, 10));
        modificationViewHolder.textViewMessage.setText(modification.getMessage());
        modificationViewHolder.textViewEtat.setText(Helper.ucfirst(modification.getEtat().getNom()));
        String letter = String.valueOf(modification.getEtat().getNom().charAt(0)).toUpperCase();
        modificationViewHolder.textViewNumber.setText(letter);
    }


    public class ModificationViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textView_date)
        TextView textViewDate;

        @Bind(R.id.textView_etat)
        TextView textViewEtat;

        @Bind(R.id.textView_message)
        TextView textViewMessage;

        @Bind(R.id.textView_number)
        TextView textViewNumber;


        public ModificationViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
