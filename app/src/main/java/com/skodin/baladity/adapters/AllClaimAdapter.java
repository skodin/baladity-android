package com.skodin.baladity.adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.AllReclamationActivity;
import com.skodin.baladity.activities.CommentActivity;
import com.skodin.baladity.activities.DetailActivity;
import com.skodin.baladity.activities.MapsActivity;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Favorie;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllClaimAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Reclamation> data = new ArrayList<>();
    AllReclamationActivity activity;
    final int VIEW_ITEM = 1;
    final int VIEW_PROG = 0;
    int visibleThreshold = 1;
    int lastVisibleItem, totalItemCount;
    boolean loading;
    OnLoadMoreListener onLoadMoreListener;

    public AllClaimAdapter(AllReclamationActivity activity, List<Reclamation> data, RecyclerView recyclerView) {
        this.activity = activity;
        this.data = data;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                         if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }

    }



    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_reclamation, parent, false);
            vh = new ClaimViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_progress_bar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ClaimViewHolder) {
            final ClaimViewHolder claimViewHolder = (ClaimViewHolder) holder;
            final Reclamation reclamation = data.get(position);

            if(reclamation.getIdMunicipalite() == 0) {
                claimViewHolder.suivre.setImageResource(R.drawable.ic_star_border_black_24dp);
            }else {
                claimViewHolder.suivre.setImageResource(R.drawable.ic_star_bleu);
            }

            claimViewHolder.textViewDate.setText(reclamation.getModifications().get(0).getDate().substring(0, 10));
            try {
                Uri uriPicture = Sdcard.checkExistenceFile(activity,Helper.getFileName(ConfService.BASE_URL + reclamation.getPhotos().get(0).getChemin()), 1);
                //teste si image existe
                if (uriPicture != null) {
                    claimViewHolder.photo1.setImageURI(uriPicture);
                } else {
                    // download image and save
                    Picasso.with(activity).load(ConfService.BASE_URL + reclamation.getPhotos().get(0).getChemin())
                            .placeholder(R.drawable.carrousel)
                            .error(R.drawable.carrousel)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                    try {
                                        claimViewHolder.photo1.setImageBitmap(bitmap);
                                        Sdcard.savePictures(activity,bitmap,Helper.getFileName(ConfService.BASE_URL + reclamation.getPhotos().get(0).getChemin()), 1);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {
                                    claimViewHolder.photo1.setImageDrawable(errorDrawable);
                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {
                                    claimViewHolder.photo1.setImageDrawable(placeHolderDrawable);
                                }
                            });
                }
            } catch (Exception e) {
                claimViewHolder.photo1.setImageResource(R.drawable.carrousel);
            }

            claimViewHolder.photo1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, DetailActivity.class);
                    intent.putExtra("reclamation", new Gson().toJson(reclamation));
                    activity.startActivity(intent);
                }
            });

            if(reclamation.getLatitude() != 0  && reclamation.getLongitude() != 0){
                claimViewHolder.maps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, MapsActivity.class);
                        Bundle b = new Bundle();
                        b.putDouble("latitude", reclamation.getLatitude());
                        b.putDouble("longitude", reclamation.getLongitude());
                        intent.putExtras(b);
                        activity.startActivity(intent);
                    }
                });
            }else {
                claimViewHolder.maps.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_map_marker_grey600_24dp));
            }



            claimViewHolder.maps.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_maps);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity,contentDesc,pos);
                    return true;
                }
            });

            claimViewHolder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, DetailActivity.class);
                    intent.putExtra("reclamation", new Gson().toJson(reclamation));
                    activity.startActivity(intent);
                }
            });

            claimViewHolder.more.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_détail);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity,contentDesc,pos);
                    return true;
                }
            });

            claimViewHolder.suivre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if(reclamation.getIdMunicipalite() == 0){
                        addSuivre(claimViewHolder, reclamation.getId());
                   }else if(reclamation.getIdMunicipalite()  == 1) {
                       deleteSuivre(claimViewHolder, reclamation.getId(), Helper.getPreferenceCitoyen(activity).getId());
                   }
                }
            });

            claimViewHolder.suivre.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_suivre);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity,contentDesc,pos);
                    return true;
                }
            });


            claimViewHolder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, CommentActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("idReclamation", reclamation.getId());
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            });


            claimViewHolder.comment.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String contentDesc = activity.getResources().getString(R.string.label_commentaire);
                    int[] pos = new int[2];
                    v.getLocationInWindow(pos);
                    Helper.showTitle(activity,contentDesc,pos);
                    return true;
                }
            });

        } else {
            final ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progressBar1)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
    public class ClaimViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.date)
        TextView textViewDate;
        @Bind(R.id.photo)
        ImageView photo1;
        @Bind(R.id.maps)
        ImageView maps;
        @Bind(R.id.detail)
        ImageView more;
        @Bind(R.id.suivre)
        ImageView suivre;
        @Bind(R.id.comment)
        ImageView comment;

        public ClaimViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    private void addSuivre(final ClaimViewHolder claimViewHolder,int id){
        BaladityAPI service = ConfService.getRetrofit(activity).create(BaladityAPI.class);
        Call<Favorie> callService = service.addFavorie(new Favorie(id, Helper.getPreferenceCitoyen(activity).getId()));
        callService.enqueue(new Callback<Favorie>() {
            @Override
            public void onResponse(Call<Favorie> call, Response<Favorie> response) {
                claimViewHolder.suivre.setImageResource(R.drawable.ic_star_bleu);
            }
            @Override
            public void onFailure(Call<Favorie> call, Throwable t) {
               //
            }
        });
    }

    private  void deleteSuivre(final ClaimViewHolder claimViewHolder,int idReclamation,int idCitoyen){
        BaladityAPI service = ConfService.getRetrofit(activity).create(BaladityAPI.class);
        Call<Favorie> callService = service.deleteFavories(idReclamation,idCitoyen);
        callService.enqueue(new Callback<Favorie>() {
            @Override
            public void onResponse(Call<Favorie> call, Response<Favorie> response) {
                claimViewHolder.suivre.setImageResource(R.drawable.ic_star_border_black_24dp);
            }

            @Override
            public void onFailure(Call<Favorie> call, Throwable t) {
              //
            }
        });
    }


}
