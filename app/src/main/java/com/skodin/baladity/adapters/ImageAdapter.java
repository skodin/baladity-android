package com.skodin.baladity.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skodin.baladity.R;
import com.skodin.baladity.models.sugar.Picture;

import java.util.List;

public class ImageAdapter extends PagerAdapter {
    private Activity activity;
    private List<Picture> data;

    public ImageAdapter(Activity activity, List<Picture> data) {
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;
        TextView textViewNum;
        LayoutInflater inflater;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.image_pager, container, false);
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        textViewNum = (TextView) viewLayout.findViewById(R.id.num_photo);
        imgDisplay.setImageURI(Uri.parse(data.get(position).getChemin()));
        textViewNum.setText((position+1)+" / "+data.size());
         container.addView(viewLayout);
        return viewLayout;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
         container.removeView((RelativeLayout) object);
    }


}