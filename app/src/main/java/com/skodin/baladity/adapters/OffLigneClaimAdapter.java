package com.skodin.baladity.adapters;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.DetailActivity;
import com.skodin.baladity.activities.UpdateNonDeclarerActivity;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.services.BaladityAPI;
import com.skodin.baladity.services.ServiceInterne;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffLigneClaimAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Claim> data = new ArrayList<>();
    Activity activity;
    RelativeLayout relativeLayout;


    public OffLigneClaimAdapter(Activity activity, List<Claim> data,RelativeLayout relativeLayout) {
        this.activity = activity;
        this.data = data;
        this.relativeLayout = relativeLayout;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public Claim getItem(int position) {
        return data.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offligne_reclamation, parent, false);
        return new offLigneClaimViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final offLigneClaimViewHolder offLigneClaimViewHolder = (offLigneClaimViewHolder) holder;
        final Claim claim = data.get(position);
        offLigneClaimViewHolder.photo1.setImageURI(Uri.parse(claim.getPictures().get(0).getChemin()));

        offLigneClaimViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteClaim(claim, position);
            }
        });

        offLigneClaimViewHolder.delete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_delete);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

        offLigneClaimViewHolder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, UpdateNonDeclarerActivity.class);
                intent.putExtra("claim", new Gson().toJson(claim));
                activity.startActivityForResult(intent,5);
            }
        });

        offLigneClaimViewHolder.detail.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_détail);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

    }


    public class offLigneClaimViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.photo)
        ImageView photo1;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.detail)
        ImageView detail;

        public offLigneClaimViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public void deleteClaim(final Claim claim, final int position) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(activity.getResources().getString(R.string.confirmation));
        dialog.setMessage(activity.getResources().getString(R.string.delete_claim));
        dialog.setPositiveButton(activity.getResources().getString(R.string.confirmer), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ServiceInterne.delete(claim.getId());
                try {
                    for (int j = 0; j < 5; j++) {
                        Sdcard.deletePictureFromStorage(Uri.parse(claim.getPictures().get(j).getChemin()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notiferAdapter(position);
                Helper.toast(activity, activity.getResources().getString(R.string.success));
            }
        });
        dialog.setNegativeButton(activity.getResources().getString(R.string.annuler), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog myDialog = dialog.create();
        myDialog.show();
        Button buttonNegative = myDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button buttonPositive = myDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonNegative.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        buttonPositive.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public void notiferAdapter(int position) {
        data.remove(position);
        notifyDataSetChanged();
        if (data.size() == 0) {
            relativeLayout.setVisibility(View.VISIBLE);
        } else {
            relativeLayout.setVisibility(View.GONE);
        }
    }


}
