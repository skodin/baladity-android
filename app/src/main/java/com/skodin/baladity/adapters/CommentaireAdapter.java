package com.skodin.baladity.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.CommentActivity;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.TimeAgo;
import com.skodin.baladity.models.Commentaire;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CommentaireAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Commentaire> data = new ArrayList<>();
    CommentActivity activity;


    public CommentaireAdapter(CommentActivity activity, List<Commentaire> data) {
        this.activity = activity;
        this.data = data;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public Commentaire getItem(int position) {
        return data.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final CommentViewHolder commentViewHolder = (CommentViewHolder) holder;
        final Commentaire commentaire = data.get(position);
        commentViewHolder.textViewUsername.setText(commentaire.getUser().getUsername());
        commentViewHolder.textViewMessage.setText(commentaire.getMessage());
        commentViewHolder.textViewDate.setText(new TimeAgo(activity).getTimeAgo(commentaire.getDate()));

        String letter=String.valueOf(commentaire.getUser().getUsername().charAt(0)).toUpperCase();
        ColorGenerator generator = ColorGenerator.MATERIAL;
        TextDrawable drawable = TextDrawable.builder().buildRound(letter,generator.getRandomColor());
        commentViewHolder.profilePhoto.setImageDrawable(drawable);
    }


    public class CommentViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textView_username)
        TextView textViewUsername;

        @Bind(R.id.textView_message)
        TextView textViewMessage;

        @Bind(R.id.textView_date)
        TextView textViewDate;

        @Bind(R.id.profile_photo)
        ImageView profilePhoto;

        public CommentViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
