package com.skodin.baladity.adapters;

public interface OnLoadMoreListener {
    void onLoadMore();
}