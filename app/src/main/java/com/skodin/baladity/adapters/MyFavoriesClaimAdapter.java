package com.skodin.baladity.adapters;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.DetailActivity;
import com.skodin.baladity.activities.MapsActivity;
import com.skodin.baladity.activities.MyFavoriesActivity;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Favorie;
import com.skodin.baladity.services.BaladityAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFavoriesClaimAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Favorie> data = new ArrayList<>();
    MyFavoriesActivity activity;
    RelativeLayout relativeLayout;

    public MyFavoriesClaimAdapter(MyFavoriesActivity activity, List<Favorie> data,RelativeLayout  relativeLayout) {
        this.activity = activity;
        this.data = data;
        this.relativeLayout=relativeLayout;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Favorie getItem(int position) {
        return data.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorie_reclamation, parent, false);
        return new FavorieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final FavorieViewHolder favorieViewHolder = (FavorieViewHolder) holder;
        final Favorie favorie = data.get(position);
        favorieViewHolder.textViewDate.setText(favorie.getProbleme().getModifications().get(0).getDate().substring(0, 10));
        try {
            Uri uriPicture = Sdcard.checkExistenceFile(activity,Helper.getFileName(favorie.getProbleme().getPhotos().get(0).getChemin()), 1);
            //teste si image existe
            if (uriPicture != null) {
                favorieViewHolder.photo1.setImageURI(uriPicture);
            } else {
                // download image and save
                Picasso.with(activity).load(ConfService.BASE_URL +favorie.getProbleme().getPhotos().get(0).getChemin())
                        .placeholder(R.drawable.carrousel)
                        .error(R.drawable.carrousel)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    favorieViewHolder.photo1.setImageBitmap(bitmap);
                                    Sdcard.savePictures(activity,bitmap, Helper.getFileName(favorie.getProbleme().getPhotos().get(0).getChemin()), 1);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                favorieViewHolder.photo1.setImageDrawable(errorDrawable);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                favorieViewHolder.photo1.setImageDrawable(placeHolderDrawable);
                            }
                        });
            }
        } catch (Exception e) {
            favorieViewHolder.photo1.setImageResource(R.drawable.carrousel);
        }

        if(favorie.getProbleme().getLatitude() != 0  && favorie.getProbleme().getLongitude() != 0){
            favorieViewHolder.maps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, MapsActivity.class);
                    Bundle b = new Bundle();
                    b.putDouble("latitude", favorie.getProbleme().getLatitude());
                    b.putDouble("longitude", favorie.getProbleme().getLongitude());
                    intent.putExtras(b);
                    activity.startActivity(intent);
                }
            });
        }else {
            favorieViewHolder.maps.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_map_marker_grey600_24dp));
        }




        favorieViewHolder.maps.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_maps);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

        favorieViewHolder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra("reclamation", new Gson().toJson(favorie.getProbleme()));
                activity.startActivity(intent);
            }
        });

        favorieViewHolder.more.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_détail);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

        favorieViewHolder.photo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra("reclamation", new Gson().toJson(favorie.getProbleme()));
                activity.startActivity(intent);
            }
        });

        favorieViewHolder.delete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String contentDesc = activity.getResources().getString(R.string.label_delete);
                int[] pos = new int[2];
                v.getLocationInWindow(pos);
                Helper.showTitle(activity, contentDesc, pos);
                return true;
            }
        });

        favorieViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog mProgressDialog = Helper.showProgress(activity);
                BaladityAPI service = ConfService.getRetrofit(activity).create(BaladityAPI.class);
                Call<Favorie> callService = service.deleteFavories(favorie.getIdReclamation(),favorie.getIdCitoyen());
                callService.enqueue(new Callback<Favorie>() {
                    @Override
                    public void onResponse(Call<Favorie> call, Response<Favorie> response) {
                        if (response.isSuccess()) {
                            data.remove(position);
                            notifyDataSetChanged();
                            if(data.size() == 0){
                                relativeLayout.setVisibility(View.VISIBLE);
                            }else {
                                relativeLayout.setVisibility(View.GONE);
                            }
                            if (mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                        } else {
                            if (mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            Helper.toast(activity, activity.getResources().getString(R.string.errorOperation));
                        }
                    }

                    @Override
                    public void onFailure(Call<Favorie> call, Throwable t) {
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Helper.toast(activity, activity.getResources().getString(R.string.request_invalid));
                    }
                });
            }
        });

    }


    public class FavorieViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.date)
        TextView textViewDate;
        @Bind(R.id.photo)
        ImageView photo1;
        @Bind(R.id.maps)
        ImageView maps;
        @Bind(R.id.detail)
        ImageView more;
        @Bind(R.id.delete)
        ImageView delete;

        public FavorieViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
