package com.skodin.baladity.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skodin.baladity.R;
import com.skodin.baladity.helpers.ConfService;
import com.skodin.baladity.helpers.Helper;
import com.skodin.baladity.helpers.Sdcard;
import com.skodin.baladity.models.Photo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

public class PhotoAdapter extends PagerAdapter {
    private Activity activity;
    private List<Photo> data;

    public PhotoAdapter(Activity activity, List<Photo> data) {
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;
        TextView textViewNum;
        LayoutInflater inflater;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.image_pager, container, false);
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        textViewNum = (TextView) viewLayout.findViewById(R.id.num_photo);
        associate(imgDisplay,position);
        textViewNum.setText((position+1)+"/"+data.size());
        container.addView(viewLayout);
        return viewLayout;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private void associate(final ImageView imageView,final int position){
            Uri uriPicture = Sdcard.checkExistenceFile(activity,Helper.getFileName(ConfService.BASE_URL +data.get(position).getChemin()), 1);
            //teste si image existe
            if (uriPicture != null) {
                imageView.setImageURI(uriPicture);
            } else {
                // download image and save
                Picasso.with(activity).load(ConfService.BASE_URL + data.get(position).getChemin())
                        .placeholder(R.drawable.carrousel)
                        .error(R.drawable.carrousel)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    imageView.setImageBitmap(bitmap);
                                    Sdcard.savePictures(activity,bitmap, Helper.getFileName(data.get(position).getChemin()), 1);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                imageView.setImageDrawable(errorDrawable);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                imageView.setImageDrawable(placeHolderDrawable);
                            }
                        });
            }

    }


}
