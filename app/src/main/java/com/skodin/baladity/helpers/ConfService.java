package com.skodin.baladity.helpers;


import android.app.Activity;
import java.io.File;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConfService {

   // public static String BASE_URL = "http://192.168.42.63/";
    public static String BASE_URL = "http://baladity.skodin.com/";

    //name of folders
    public static final String directoryPicturesUpload = "Uploads";
    public static final String directoryApp = "Baladity";
    public static final String directoryPicturesCitoyen = "citoyen";
    public static final String directoryPicturesClaim = "claim";
    public static final String directoryPicturesClaimInternal = "claimInterne";

    public static final int REQUEST_IMAGE_CAMERA = 0;
    public static final int REQUEST_IMAGE_CALLERY = 1;

    //get instance of retrofit
    public static Retrofit getRetrofit(Activity activity) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHttpClient(activity))
                .build();
        return retrofit;
    }

    //make header for All requests
    public static OkHttpClient getHttpClient(final Activity activity) {
        OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Request request;
                                if (Helper.getPreferenceCitoyen(activity) != null) {
                                    request = chain.request().newBuilder()
                                            .addHeader("Authorization", " Bearer " + Helper.getPreferenceCitoyen(activity).getToken())
                                            .addHeader("role", "citoyen").build();
                                } else {
                                    request = chain.request().newBuilder()
                                            .addHeader("Authorization", " Bearer ")
                                            .addHeader("role", "citoyen").build();
                                }
                                return chain.proceed(request);
                            }
                        }).build();
        return defaultHttpClient;
    }

    //convert string to requestBody
    public static RequestBody toRequestBody(String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body;
    }

    //convert file to requestBody
    public static RequestBody toRequestBodyFile(File value) {
        RequestBody body = RequestBody.create(MediaType.parse("image/*"), value);
        return body;
    }

}
