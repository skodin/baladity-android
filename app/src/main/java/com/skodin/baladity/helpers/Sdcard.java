package com.skodin.baladity.helpers;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Sdcard {


    //dossier Baladity
    public static File directoryApp(Activity activity) {
        //File sdcard = Environment.getExternalStorageDirectory();
        File sdcard = activity.getExternalFilesDir(null);
        File folderApp = new File(sdcard.getAbsoluteFile(), ConfService.directoryApp);
        folderApp.mkdir();
        return folderApp;
    }

    //dossier citoyen
    public static File directoryPicturesCitoyen(Activity activity) {
        File storage = directoryApp(activity);
        File folderPicturesCitoyen = new File(storage.getAbsoluteFile(), ConfService.directoryPicturesCitoyen);
        folderPicturesCitoyen.mkdir();
        return folderPicturesCitoyen;
    }


    //dossier claim
    public static File directoryPicturesClaim(Activity activity) {
        File storage = directoryApp(activity);
        File folderPicturesCitoyen = new File(storage.getAbsoluteFile(), ConfService.directoryPicturesClaim);
        folderPicturesCitoyen.mkdir();
        return folderPicturesCitoyen;
    }

    //dossier upload
    public static File directoryOfUploadPictures(Activity activity) {
        File storage = directoryApp(activity);
        File folderUpload = new File(storage.getAbsoluteFile(), ConfService.directoryPicturesUpload);
        folderUpload.mkdir();
        return folderUpload;
    }

    //dossier directoryPicturesClaimInternal
    public static File directoryOfUploadPicturesClaimInternal(Activity activity) {
        File storage = directoryApp(activity);
        File folderUpload = new File(storage.getAbsoluteFile(), ConfService.directoryPicturesClaimInternal);
        folderUpload.mkdir();
        return folderUpload;
    }

    //delete Picture
    public static void deletePictureFromStorage(Uri fileUri) {
        File file = new File(fileUri.getPath());
        if (file.isFile()) {
            file.delete();
        }
    }

    // delete All Pictures uploaded
    public static void deleteAllPicturesUploaded(Activity activity) {
        File folder = directoryOfUploadPictures(activity);
        String[] myFiles;
        myFiles = folder.list();
        for (int i = 0; i < myFiles.length; i++) {
            File myFile = new File(folder, myFiles[i]);
            myFile.delete();
        }
    }

    // storage internal
    public static void savePictures(Activity activity,Bitmap bitmap, String fileName, int flag) {
        File folder;
        switch (flag) {
            case 0:
                folder = directoryPicturesCitoyen(activity);
                break;
            case 1:
                folder = directoryPicturesClaim(activity);
                break;
            default:
                folder = directoryApp(activity);
        }
        File file = new File(folder.getAbsoluteFile() + File.separator + fileName);
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // folder temp
    public static Uri saveUploadedPicturesToSdcard(Activity activity,Bitmap bitmap, String filename) {
        Uri fileUri;
        File folder = directoryOfUploadPictures(activity);
        File file = new File(folder.getAbsoluteFile() + File.separator + "img_" + filename + ".jpg");
        fileUri = Uri.fromFile(file);
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG,70, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileUri;
    }



    public static Uri movefile(Uri uri, Activity activity) {
        File folderFrom = directoryOfUploadPictures(activity);
        File folderTo = directoryOfUploadPicturesClaimInternal(activity);
        File from = new File(folderFrom.getAbsolutePath() + "/" + getFileName(uri, activity));
        File to = new File(folderTo + "/" + getFileName(uri, activity));
        from.renameTo(to);
        return Uri.fromFile(to);

    }

    public static String getFileName(Uri uri, Activity activity) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                try {
                    cursor.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static Uri savePicturesToSdcardSugar(Activity activity,Bitmap bitmap, String filename) {
        Uri fileUri;
        File folder = directoryOfUploadPicturesClaimInternal(activity);
        File file = new File(folder.getAbsoluteFile() + File.separator + "img_" + filename + ".jpg");
        fileUri = Uri.fromFile(file);
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileUri;
    }


    public static Uri checkExistenceFile(Activity activity,String fileName, int flag) {
        Uri fileUri = null;
        File folder;
        switch (flag) {
            case 0:
                folder = directoryPicturesCitoyen(activity);
                break;
            case 1:
                folder = directoryPicturesClaim(activity);
                break;
            default:
                folder = directoryApp(activity);
        }

        File picture = new File(folder.getAbsoluteFile() + File.separator + fileName);
        if (picture.isFile()) {
            fileUri = Uri.fromFile(picture);
        }
        return fileUri;
    }


    public static String getRealPathFromURI(Uri contentURI,Context context) {
        String path= contentURI.getPath();
        try {
            Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
          try {
              cursor.moveToFirst();
          }catch (Exception e){
              e.printStackTrace();
          }
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();
            cursor = context.getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
           try {
               cursor.moveToFirst();
           }catch (Exception e){
               e.printStackTrace();
           }
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
        }
        catch(Exception e)
        {
            return path;
        }
        return path;
    }


    //test
    public static File getOutputMediaFile(Activity activity){
        File mediaStorageDir = new File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "CameraDemo");
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
    }


}
