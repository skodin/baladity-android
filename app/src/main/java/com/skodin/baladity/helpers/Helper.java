package com.skodin.baladity.helpers;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.skodin.baladity.R;
import com.skodin.baladity.activities.MainActivity;
import com.skodin.baladity.models.Citoyen;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {


    public static void showTitle(Activity activity,String contentDesc,int[] pos){
        Toast t = Toast.makeText(activity, contentDesc,Toast.LENGTH_SHORT);
        t.setGravity(Gravity.TOP | Gravity.LEFT, pos[0] - ((contentDesc.length() / 2) * 12), pos[1] - 128);
        t.show();
    }

    public static void deletePreferenceCitoyen(Activity activity){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
        settings.edit().remove("citoyen").commit();
    }

  /*
    public static void ze(Activity activity){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
        settings.edit().remove("citoyen").commit();
    }
   */

    public static void savePreferenceCitoyen(Activity activity, Citoyen citoyen) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
        final SharedPreferences.Editor editor1 = settings.edit();
        Gson gson = new Gson();
        String json = gson.toJson(citoyen);
        editor1.putString("citoyen", json);
        editor1.commit();
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }

    public static Citoyen getPreferenceCitoyen(Activity activity) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
        Gson gson = new Gson();
        String json = settings.getString("citoyen", "");
        return gson.fromJson(json, Citoyen.class);
    }


    public static ProgressDialog showProgress(Activity activity) {
        ProgressDialog mProgressDialog=new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(activity.getResources().getString(R.string.msg_progress_dialog));
        mProgressDialog.show();
        return mProgressDialog;
    }


    public static void toast(Activity activity, String s) {
        Toast.makeText(activity, s, Toast.LENGTH_LONG).show();
    }

    public static boolean testConnexionInternet(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            NetworkInfo.State networkState = networkInfo.getState();
            if (networkState.compareTo(networkState.CONNECTED) == 0) {
                return true;
            }
        }
        return false;
    }

    public static String getDateSystem() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String ucfirst(String chaine){
        return chaine.substring(0, 1).toUpperCase()+ chaine.substring(1).toLowerCase();
    }


    public  static String getFileName(String url){
        return  url.substring(url.lastIndexOf('/')+1, url.length());
    }


}
