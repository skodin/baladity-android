package com.skodin.baladity.services;


import com.skodin.baladity.models.sugar.Category;
import com.skodin.baladity.models.sugar.Claim;
import com.skodin.baladity.models.sugar.Municipality;
import com.skodin.baladity.models.sugar.Picture;

import java.util.List;

public class ServiceInterne {


    //save Picture
    public static long savePicture(Picture picture) {
        return picture.save();
    }

    //save Picture
    public static void updatePicture(Picture picture,String chemin) {
        Picture p=Picture.findById(Picture.class, picture.getId());
        p.setChemin(chemin);
        p.save();
    }

    //save claim
    public static Claim saveClaim(Claim claim) {
        claim.save();
        return claim;
    }

    //update claim

    public static void updateClaim(Claim claim){
        Claim c = Claim.findById(Claim.class, claim.getId());
        c.setDescription(claim.getDescription());
        c.setIdMunicipalite(claim.getIdMunicipalite());
        c.setIdCategorieProleme(claim.getIdCategorieProleme());
        c.setFlag(claim.getFlag());
        c.save();
    }

    //get claims
    public static List<Claim> getAllClaim() {
        return Claim.listAll(Claim.class);
    }


    public static void delete(long id){
        Claim claim = Claim.findById(Claim.class, id);
        claim.delete();
    }






}
