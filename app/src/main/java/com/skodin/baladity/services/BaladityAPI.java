package com.skodin.baladity.services;


import com.skodin.baladity.models.Categoriesprobleme;
import com.skodin.baladity.models.Citoyen;
import com.skodin.baladity.models.Commentaire;
import com.skodin.baladity.models.Contact;
import com.skodin.baladity.models.Etat;
import com.skodin.baladity.models.Favorie;
import com.skodin.baladity.models.Modification;
import com.skodin.baladity.models.Paginate;
import com.skodin.baladity.models.Photo;
import com.skodin.baladity.models.Reclamation;
import com.skodin.baladity.models.ResponseObject;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaladityAPI {

    /*-----------------------------------------user-----------------------------*/
    @POST("loginCitoyen")
    Call<Citoyen> login(@Body Citoyen citoyen);

    @GET("logout")
    Call<ResponseObject> logout();

    @Multipart
    @POST("registerCitoyen")
    Call<Citoyen> register(@PartMap Map<String,RequestBody> params);

    @Multipart
    @POST("updateCitoyen")
    Call<Citoyen> updateCompte(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("updateImageUser")
    Call<ResponseObject> updateImageUser(@Part("id") RequestBody id, @Part("photo\"; filename=\"pp.png\" ") RequestBody photo);


   /*-----------------------------------------claim-----------------------------------*/
    @Multipart
    @POST("reclamations")
    Call<Reclamation> declarer(@PartMap Map<String, RequestBody> params);

    @GET("citoyenReclamation/{id}")
    Call<Paginate> getMyReclamations(@Path("id") int citoyenId,@Query("page") int page);

    @DELETE("reclamations/{id}")
    Call<ResponseObject> deleteReclamation(@Path("id") int idReclamation);

    @FormUrlEncoded
    @PUT("reclamations/{id}")
    Call<ResponseObject> updateReclamation(@Path("id") int idReclamation, @FieldMap Map<String, String> params);

    @Multipart
    @POST("photos")
    Call<Photo> updatePhoto(@Part("id") RequestBody id, @Part("chemin\"; filename=\"pp.png\" ") RequestBody chemin);

    @Multipart
    @POST("addPhoto")
    Call<Photo> addPhoto(@Part("chemin\"; filename=\"pp.png\" ") RequestBody chemin,@Part("idReclamation") RequestBody idReclamation);


    @GET("showByDescription/{description}/{idCitoyen}")
    Call<List<Reclamation>> getReclamationsByDescription(@Path("description") String description,@Path("idCitoyen") int idCitoyen);

    @GET("filterByCategoryAndMunicipality/{idEtat}/{idCategory}/{idCitoyen}")
    Call<Paginate> filterByCategoryAndMunicipality(@Path("idEtat") int idEtat,@Path("idCategory") int idCategory,@Path("idCitoyen") int idCitoyen,@Query("page") int page);

    /*----------------------------------commentaires------------------------------------------------*/
    @POST("commentaires")
    Call<Commentaire> sendCommentaires(@Body Commentaire commentaire);

    @GET("showComments/{id}")
    Call<List<Commentaire>> getCommentaires(@Path("id") int id);


    /*------------------------------------ fill spinners  ----------------------------------*/

    @GET("categorieProblemes")
    Call<List<Categoriesprobleme>> getAllCategorieProblemes();


    /*-----------------------------filtrer par état ----------------------------------------------*/
    @GET("getEtats")
    Call <List<Etat>> getAllEtats();



    /*-----------------------------------favories--------------------------------------------*/
    @POST("favories")
    Call<Favorie> addFavorie(@Body Favorie favorie);

    @GET("favories/{id}")
    Call<List<Favorie>> getFavories(@Path("id") int id);

    @DELETE("deleteFavories/{idReclamation}/{idCitoyen}")
    Call<Favorie> deleteFavories(@Path("idReclamation") int idReclamation,@Path("idCitoyen") int idCitoyen);



    /*-------------------------------get Modifications By idClaim------------------------------------------------------*/
    @GET("modifications/{id}")
    Call<List<Modification>> getModifications(@Path("id") int id);


    /*------------------------------------send mail--------------------------------------*/
    @POST("contacts")
    Call<Contact> sendMail(@Body Contact contact);



}
